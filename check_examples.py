#!/usr/bin/python3

import sys, os
import subprocess

def get_examples():
    examples = []
    for example in os.listdir("examples"):
        if not example.endswith(".rs"):
            continue
        examples.append(example.replace(".rs",""))
    return examples

def run_example(example_name, pring_log: bool):
    print(f"Checking example {example_name}.", end="")
    status_code = os.system(f"bash -c 'cargo run -q --example {example_name} --features=full &> examples/{example_name}.log'")

    return (status_code, f"examples/{example_name}.log")

def run_examples(examples_in_args, print_logs: bool):
    os.environ["RUSTFLAGS"] = "-Awarnings"
    status_code = 0
    examples = sorted(get_examples())
    success, failed = 0, 0
    failed_examples: list[str] = []
    if "all" in examples_in_args:
        pass
    else:
        examples_buf = []
        for example in examples_in_args:
            if example in examples:
                examples_buf.append(example)
        examples = examples_buf
    max_len = len(max(examples, key=len))

    for example in examples:
        status_code, log_file = run_example(example, print_logs)
        print(" "*((18+max_len+2)-(18+len(example))), end="")
        if status_code == 0:
            print("[OK]")
            success += 1
        else:
            print("[ERR]")
            failed += 1
            failed_examples.append(example)
        if print_logs:
            os.system(f"cat {log_file}")

    print()
    print("Success:", success)
    print("Failed:", failed, "" if failed==0 else failed_examples)

def clean():
    os.system(r"find . -name target -type d -exec rm -rf '{}' ';' &> /dev/null")
    os.system(r"find . -regex '.*\(\.log\|Cargo\.lock\)$' -type f -exec rm '{}' ';' &> /dev/null")

def main():
    args = sys.argv
    runner = args.pop(0)
    print_logs = False
    if "-p" in args:
        args.pop(args.index("-p"))
        print_logs = True

    if len(args) == 0:
        print("No command provided.")
        exit(-1)
    else:
        command = args[0]
        if command == "clean":
            clean()
        elif command == "run":
            if len(args) > 1:
                args.pop(0)
                run_examples(args, print_logs)
            else:
                print(f"Example name not provided. e.g.\n{runner} run cpu (or whatever)")
                print("Examples:", get_examples())
        else:
            print("Unknown command provided")
            exit(-1)

if __name__ == "__main__":
    main()
