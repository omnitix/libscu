#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/ioctl.h>

int get_terminal_size(int *cols, int *rows) {
  int tty_fd = open("/dev/tty", O_EVTONLY | O_NONBLOCK);
  if (tty_fd == -1)
    return errno;

  struct winsize ws;
  int result = ioctl(tty_fd, TIOCGWINSZ, &ws);
  close(tty_fd);

  if (result == -1)
    return errno;

  *cols = ws.ws_col;
  *rows = ws.ws_row;

  return 0;
}