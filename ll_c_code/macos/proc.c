#include <errno.h>
#include <stdlib.h>

#include <mach/mach.h>
#include <sys/sysctl.h>

#include <sys/proc.h>

int get_parent_pid(int pid, uint *ppid_buf) {
  int opts[] = {CTL_KERN, KERN_PROC, KERN_PROC_PID, pid};

  struct kinfo_proc proc;
  size_t proc_len = sizeof(struct kinfo_proc);

  if (sysctl(opts, 4, &proc, &proc_len, NULL, 0) != 0)
    return errno;

  *ppid_buf = proc.kp_eproc.e_ppid;

  return 0;
}

int get_command(int pid, char *comm) {
  int opts[] = {CTL_KERN, KERN_PROC, KERN_PROC_PID, pid};

  struct kinfo_proc proc;
  size_t proc_len = sizeof(struct kinfo_proc);

  if (sysctl(opts, 4, &proc, &proc_len, NULL, 0) != 0)
    return errno;

  strcpy(comm, proc.kp_proc.p_comm);

  return 0;
}

int list_pids(int *pids_buf) {
  int opts[] = {CTL_KERN, KERN_PROC, KERN_PROC_ALL};
  size_t len = 0;

  if (sysctl(opts, 3, NULL, &len, NULL, 0) != 0)
    return errno;

  struct kinfo_proc *proc_list = malloc(len);

  int proc_size = len / sizeof(struct kinfo_proc);

  if (sysctl(opts, 3, proc_list, &len, NULL, 0) != 0)
    return errno;

  for (int i = 0; i < proc_size; i++)
    pids_buf[i] = proc_list[i].kp_proc.p_pid;

  return 0;
}
