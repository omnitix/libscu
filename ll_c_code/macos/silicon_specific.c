#include <string.h>
#include <mach/mach.h>

#include <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOKitLib.h>

#if (MAC_OS_X_VERSION_MIN_REQUIRED < 120000)
#define kIOMainPortDefault kIOMasterPortDefault
#endif

const char* silicon_freq(uint32_t *cur) {
    uint32_t max;
    kern_return_t status;
    CFDictionaryRef matching = NULL;
    CFTypeRef pCoreRef = NULL;
    io_iterator_t iter = 0;
    io_registry_entry_t entry = 0;
    io_name_t name;

    matching = IOServiceMatching("AppleARMIODevice");
    if (matching == 0) {
        return "IOServiceMatching call failed, 'AppleARMIODevice' not found";
    }

    status = IOServiceGetMatchingServices(kIOMainPortDefault, matching, &iter);
    if (status != KERN_SUCCESS) {
        return "IOServiceGetMatchingServices call failed";
    }

    while ((entry = IOIteratorNext(iter)) != 0) {
        status = IORegistryEntryGetName(entry, name);
        if (status != KERN_SUCCESS) {
            IOObjectRelease(entry);
            continue;
        }
        if (strcmp(name, "pmgr") == 0) {
            break;
        }
        IOObjectRelease(entry);
    }

    if (entry == 0) {
        return "'pmgr' entry was not found in AppleARMIODevice service";
    }

    pCoreRef = IORegistryEntryCreateCFProperty(
        entry, CFSTR("voltage-states5-sram"), kCFAllocatorDefault, 0);
    if (pCoreRef == NULL) {
        return "'voltage-states5-sram' property not found";
    }

    size_t pCoreLength = CFDataGetLength(pCoreRef);
    if (pCoreLength < 8) {
        return "expected 'voltage-states5-sram' buffer to have at least size 8";
    }

    CFDataGetBytes(pCoreRef, CFRangeMake(pCoreLength - 8, 4), (UInt8 *) &max);

    *cur = max / 1000 / 1000;

    CFRelease(pCoreRef);
    IOObjectRelease(iter);
    IOObjectRelease(entry);

    return NULL;
}
