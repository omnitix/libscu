#include <pwd.h>
#include <unistd.h>

const char *get_pwd_shell() {
  struct passwd *p = getpwuid(getuid());
  return p->pw_shell;
}

const char *get_pwd_homedir() {
  struct passwd *p = getpwuid(getuid());
  return p->pw_dir;
}