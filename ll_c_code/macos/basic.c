/*
  Why used native C code instead of rust wrapper ?
  idk wrapper just didn't worked
  i tried several times
*/

#include <mach/clock.h>
#include <mach/mach.h>
#include <stdlib.h>
#include <sys/sysctl.h>
#include <sys/time.h>
#include <unistd.h>

uint32_t get_page_size() {
  uint32_t page_size;

  size_t len = sizeof(page_size);

  return sysctl((int[]){CTL_HW, HW_PAGESIZE}, 2, &page_size, &len, NULL, 0) == 0
             ? page_size
             : 0;
}

/* RAM */
uint64_t get_used_memory() {
  mach_msg_type_number_t count = HOST_VM_INFO64_COUNT;
  vm_statistics64_data_t vmstat;

  uint32_t page_size = get_page_size();
  if (page_size == 0) {
    return 0;
  }

  if (host_statistics64(mach_host_self(), HOST_VM_INFO64,
                        (host_info64_t)(&vmstat), &count) == KERN_SUCCESS) {
    return ((uint64_t) + vmstat.active_count + vmstat.inactive_count +
            vmstat.speculative_count + vmstat.wire_count +
            vmstat.compressor_page_count - vmstat.purgeable_count -
            vmstat.external_page_count) *
           get_page_size();
  } else {
    return 0;
  }
}
int get_swap_info(uint64_t *swap_total, uint64_t *swap_used) {
  struct xsw_usage xsw;
  size_t size = sizeof(xsw);

  if (sysctl((int[]){CTL_VM, VM_SWAPUSAGE}, 2, &xsw, &size, NULL, 0) != 0)
    return -1;

  *swap_total = xsw.xsu_total;
  *swap_used = xsw.xsu_used;

  return 0;
}

const char *string_value(int name_major, int name_minor) {
  char *result[128];
  size_t len = sizeof(result);

  if (sysctl((int[]){name_major, name_minor}, 2, &result, &len, NULL, 0) ==
      0) {
    char *vc = malloc(len + 1);
    if (vc != NULL) {
      memcpy(vc, result, len + 1);
      return vc;
    }
  }

  return NULL;
}

/* TIME */
uint64_t get_time_now() {
  struct timespec ts;

  clock_gettime(CLOCK_REALTIME, &ts);

  return (uint64_t)ts.tv_sec;
}

uint64_t get_uptime_secs() {
  int name[2] = {CTL_KERN, KERN_BOOTTIME};

  struct timeval uptime;
  size_t len = sizeof(uptime);

  return sysctl(name, 2, &uptime, &len, NULL, 0) == 0
             ? (get_time_now() - (uint64_t)uptime.tv_sec)
             : 0;
}

