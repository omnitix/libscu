#[cfg(feature = "mounts")]
use super::libc::StatFS;
use super::libc::{uname as _uname, UtsName};

use std::{
    ffi::{c_char, CStr, CString},
    io::{Error, Result},
};

pub fn uname_kernel_name() -> Result<String> {
    unsafe {
        uname().map(|utsname| {
            CStr::from_ptr(utsname.sysname.as_ptr())
                .to_string_lossy()
                .into_owned()
        })
    }
}

pub fn uname_kernel_release() -> Result<String> {
    unsafe {
        uname().map(|utsname| {
            CStr::from_ptr(utsname.release.as_ptr())
                .to_string_lossy()
                .into_owned()
        })
    }
}

pub fn uname_machine_arch() -> Result<String> {
    unsafe {
        uname().map(|utsname| {
            CStr::from_ptr(utsname.machine.as_ptr())
                .to_string_lossy()
                .into_owned()
        })
    }
}

fn uname() -> Result<Box<UtsName>> {
    unsafe {
        let mut utsname: Box<UtsName> = Box::new(std::mem::zeroed());

        if _uname(utsname.as_mut()) != 0 {
            return Err(Error::last_os_error());
        }

        Ok(utsname)
    }
}

#[cfg(feature = "mounts")]
pub fn statfs(path: &str) -> Result<StatFS> {
    let mut statfs_result: StatFS = StatFS::default();
    let result: std::ffi::c_int;
    unsafe {
        result = super::libc::statfs(
            CString::new(path)?.as_ptr() as *const c_char,
            &mut statfs_result,
        );
    }
    if result == 0 {
        Ok(statfs_result)
    } else {
        Err(std::io::Error::last_os_error())
    }
}
