#![allow(non_camel_case_types)]

use super::unix::libc::{fsid_t, uid_t, Passwd};

use std::ffi::{c_char, c_int, c_uint};
#[cfg(feature = "mounts")]
use std::ffi::{c_long, c_short};

type uint64_t = u64;

pub const CTL_KERN: c_int = 1;
pub const CTL_HW: c_int = 6;

pub const HW_MODEL: c_int = 2;
pub const HW_NCPU: c_int = 3;
pub const HW_CPU_FREQ: c_int = 15;
pub const HW_MEMSIZE: c_int = 24;

pub const KERN_VERSION: c_int = 4;
pub const KERN_HOSTNAME: c_int = 10;
pub const KERN_PROC: c_int = 14;

pub const KERN_PROC_ALL: c_int = 0;

pub const PID_MAX: usize = 30000;
pub const MAXCOMLEN: usize = 16;

extern "C" {
    pub fn getuid() -> c_uint;
    pub fn getpwuid(uid: c_int) -> *mut Passwd;
    pub fn uname(uts: *mut UtsName) -> c_int;
    #[cfg(feature = "mounts")]
    pub fn statfs(path: *const c_char, buf: *mut StatFS) -> c_int;

    // ll_c_code/macos/basic.c
    pub fn get_used_memory() -> uint64_t;
    pub fn get_swap_info(total: *mut uint64_t, used: *mut uint64_t) -> c_int;
    pub fn get_uptime_secs() -> uint64_t;
    // ll_c_code/macos/silicon_specific.c
    #[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
    pub fn silicon_freq(mhz: &mut u32) -> *const c_char;
}

// ll_c_code/macos/proc.c
#[cfg(feature = "proc")]
extern "C" {
    pub fn get_parent_pid(pid: c_int, ppid_buf: *mut c_uint) -> c_int;
    pub fn get_command(pid: c_int, comm: *mut c_char) -> c_int;
    pub fn list_pids(pids: *mut c_int) -> c_int;
}

// ll_c_code/macos/user.c
#[cfg(feature = "users")]
extern "C" {
    pub fn get_pwd_shell() -> *const c_char;
    pub fn get_pwd_homedir() -> *const c_char;
}

// ll_c_code/macos/term_size.c
#[cfg(feature = "terminal")]
extern "C" {
    pub fn get_terminal_size(cols: *mut c_int, rows: *mut c_int) -> c_int;
}

const _SYS_NAMELEN: usize = 256;
#[repr(C)]
pub struct UtsName {
    pub sysname: [c_char; _SYS_NAMELEN],
    pub nodename: [c_char; _SYS_NAMELEN],
    pub release: [c_char; _SYS_NAMELEN],
    pub version: [c_char; _SYS_NAMELEN],
    pub machine: [c_char; _SYS_NAMELEN],
}

#[cfg(feature = "mounts")]
#[repr(C)]
pub struct StatFS {
    pub f_otype: c_short,
    pub f_oflags: c_short,
    pub f_bsize: c_long,
    pub f_iosize: c_long,
    pub f_blocks: c_long,
    pub f_bfree: c_long,
    pub f_bavail: c_long,
    pub f_files: c_long,
    pub f_ffree: c_long,
    pub f_fsid: fsid_t,
    pub f_owner: uid_t,
    pub f_reserved1: c_short,
    pub f_type: c_short,
    pub f_flags: c_long,
    pub f_reserved2: [c_long; 2],
    pub f_fstypename: [c_char; 15],
    pub f_mntonname: [c_char; 90],
    pub f_mntfromname: [c_char; 90],
    pub f_reserved3: c_char,
    pub f_reserved4: [c_long; 4],
}

#[cfg(feature = "mounts")]
impl Default for StatFS {
    fn default() -> Self {
        Self {
            f_otype: 0,
            f_oflags: 0,
            f_bsize: 0,
            f_iosize: 0,
            f_blocks: 0,
            f_bfree: 0,
            f_bavail: 0,
            f_files: 0,
            f_ffree: 0,
            f_fsid: fsid_t::default(),
            f_owner: 0,
            f_reserved1: 0,
            f_type: 0,
            f_flags: 0,
            f_reserved2: [0; 2],
            f_fstypename: [0; 15],
            f_mntonname: [0; 90],
            f_mntfromname: [0; 90],
            f_reserved3: 0,
            f_reserved4: [0; 4],
        }
    }
}
