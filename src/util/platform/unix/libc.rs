#![cfg(target_family = "unix")]
#![allow(non_camel_case_types)]

use std::ffi::{c_char, c_int};

pub const TIOCGWINSZ: u64 = 0x5413;
pub const STDIN_FILENO: i32 = 0;
pub const STDOUT_FILENO: i32 = 1;

#[cfg(feature = "locale")]
pub const LC_ALL: c_int = 6;

pub type uid_t = u32;

extern "C" {
    pub fn ioctl(fs: i32, requets: u64, argp: *mut WinSize) -> i32;
    pub fn isatty(fd: i32) -> i8;
    pub fn gethostname(hostname_buf: *mut c_char, length: usize) -> c_int;
    #[cfg(feature = "locale")]
    pub fn setlocale(category: c_int, locale: *const c_char) -> *const c_char;
}

#[repr(C)]
pub struct WinSize {
    pub ws_row: u16,
    pub ws_col: u16,
    pub ws_xpixel: u16,
    pub ws_ypixel: u16,
}

// used only on MacOS ¯\_(ツ)_/¯
#[allow(dead_code)]
#[repr(C)]
pub struct Passwd {
    pub pw_name: *mut c_char,  /* username */
    pw_passwd: *mut c_char,    /* user password */
    pub pw_uid: c_int,         /* user ID */
    pub pw_gid: c_int,         /* group ID */
    pw_gecos: *mut c_char,     /* user information */
    pub pw_dir: *mut c_char,   /* home directory */
    pub pw_shell: *mut c_char, /* shell program */
}

#[repr(C)]
#[derive(Default)]
pub struct fsid_t {
    __val: [c_int; 2],
}
