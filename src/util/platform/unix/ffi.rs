use super::libc::gethostname as _gethostname;

use std::ffi::{c_char, CStr};

pub fn gethostname() -> std::io::Result<String> {
    unsafe {
        let mut buf: [c_char; 65] = std::mem::zeroed();

        if _gethostname(&mut buf as *mut c_char, 65) == 0 {
            Ok(CStr::from_ptr(buf.as_ptr()).to_string_lossy().into_owned())
        } else {
            Err(std::io::Error::last_os_error())
        }
    }
}

pub fn isatty(fd: i32) -> i8 {
    unsafe { super::libc::isatty(fd) }
}

#[cfg(feature = "locale")]
pub fn setlocale(category: std::ffi::c_int, locale: &str) -> std::io::Result<String> {
    use std::io::{Error, ErrorKind};
    let mut result: String;

    let Ok(c_string_locale) = std::ffi::CString::new(locale) else {
        return Err(Error::new(
            ErrorKind::InvalidData,
            "expected locale without \0 terminator",
        ));
    };

    unsafe {
        let locale: *const c_char = super::libc::setlocale(category, c_string_locale.as_ptr());
        if locale.is_null() {
            Err(Error::new(ErrorKind::Other, "request cannot be honored"))
        } else {
            Ok(CStr::from_ptr(locale).to_string_lossy().to_string())
        }
    }
}
