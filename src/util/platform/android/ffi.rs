#![cfg(target_os = "android")]

use super::libc::__system_property_get;

use std::{
    ffi::{CStr, CString},
    io::{Error, ErrorKind, Result},
    os::raw::c_char,
};

fn into_string(ptr: *mut c_char) -> String {
    unsafe { CStr::from_ptr(ptr).to_string_lossy().into_owned() }
}

pub fn getprop(prop: &str) -> Result<String> {
    if !prop.is_ascii() {
        return Err(Error::new(
            ErrorKind::InvalidData,
            format!("prop [{prop}] is not ascii"),
        ));
    }
    let val: *mut c_char = CString::default().into_raw();

    let len = unsafe { __system_property_get(CString::new(prop).unwrap().into_raw(), val) };

    if len > 0 {
        Ok(into_string(val))
    } else {
        Err(Error::new(
            ErrorKind::NotFound,
            format!("property [{prop}] not found"),
        ))
    }
}
