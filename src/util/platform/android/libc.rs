#![cfg(target_os = "android")]

use std::ffi::{c_char, c_int};

#[link(name = "c")]
extern "C" {
    pub fn __system_property_get(name: *const c_char, value: *mut c_char) -> c_int;
}
