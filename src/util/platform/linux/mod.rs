#![cfg(any(target_os = "linux", target_os = "android"))]
#![allow(unused)]

mod ffi;
pub mod libc;

pub use ffi::*;
