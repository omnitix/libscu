#![cfg(any(target_os = "linux", target_os = "android"))]
#![allow(non_camel_case_types)]

use super::super::unix::libc::fsid_t;

use std::ffi::{c_char, c_int, c_uint, c_ushort};

extern "C" {
    pub fn get_nprocs_conf() -> c_int;
    pub fn sysinfo(info: *mut SysInfo) -> c_int;
    pub fn uname(uts: *mut UtsName) -> c_int;
    #[cfg(feature = "mounts")]
    pub fn statfs(path: *const c_char, buf: *mut StatFS) -> c_int;
}

#[repr(C)]
pub struct UtsName {
    pub sysname: [c_char; 65],
    pub nodename: [c_char; 65],
    pub release: [c_char; 65],
    pub version: [c_char; 65],
    pub machine: [c_char; 65],
    pub domainname: [c_char; 65],
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct SysInfo {
    pub uptime: i64,
    pub loads: [u64; 3],
    pub totalram: u64,
    pub freeram: u64,
    pub sharedram: u64,
    pub bufferram: u64,
    pub totalswap: u64,
    pub freeswap: u64,
    pub procs: c_ushort,
    pub pad: c_ushort,
    pub totalhigh: u64,
    pub freehigh: u64,
    pub mem_unit: c_uint,
    pub _f: [c_char; 0],
}

type __fsword_t = i64;
type fsfilcnt_t = u64;
type fsblkcnt_t = u64;
#[cfg(any(target_os = "linux", target_os = "android"))]
#[repr(C)]
#[derive(Default)]
pub struct StatFS {
    pub f_type: __fsword_t,
    pub f_bsize: __fsword_t,
    pub f_blocks: fsblkcnt_t,
    pub f_bfree: fsblkcnt_t,
    pub f_bavail: fsblkcnt_t,
    pub f_files: fsfilcnt_t,
    pub f_ffree: fsfilcnt_t,
    pub f_fsid: fsid_t,
    pub f_namelen: __fsword_t,
    pub f_frsize: __fsword_t,
    f_spare: [__fsword_t; 5],
}
