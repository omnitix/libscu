#![cfg(feature = "disk")]

use crate::hardware::disk::Filesystem;

pub const FILESYSTEM_MBR_BYTES: [(Filesystem, u8); 10] = [
    (Filesystem::BTRFS, 0x83),
    (Filesystem::exFAT_NTFS_HPFS, 0x07),
    (Filesystem::ext2, 0x83),
    (Filesystem::ext3, 0x83),
    (Filesystem::ext4, 0x83),
    (Filesystem::FAT12, 0x01),
    (Filesystem::FAT16, 0x04),
    (Filesystem::FAT32, 0x0B),
    (Filesystem::FAT32, 0x0C),
    (Filesystem::EFI, 0xef),
];
