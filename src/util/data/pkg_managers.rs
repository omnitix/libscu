#![cfg(feature = "packages")]

#[cfg(target_os = "macos")]
pub const MACOS_PACKAGE_MANAGER_LIST_COMMAND: [(&str, &str); 2] =
    [("brew", ""), ("port", "installed")];
