mod des;
mod filesystem_types;
pub mod filesystems;
mod macos_device;
pub mod pci_ids;
mod pkg_managers;
mod shells;
mod terminals;
mod wms;

#[cfg(feature = "graphics")]
pub use des::{DesktopEnvironment, KNOWN_DESKTOP_ENVIRONMENTS};
#[cfg(any(feature = "shell", feature = "users"))]
pub use shells::KNOWN_SHELLS;
#[cfg(feature = "graphics")]
pub use wms::{WindowManager, KNOWN_WINDOW_MANAGERS};

#[cfg(all(target_os = "linux", feature = "terminal"))]
pub use terminals::LINUX_KNOWN_TERMINALS;
#[cfg(all(target_os = "macos", feature = "terminal"))]
pub use terminals::MACOS_KNOWN_TERMINALS;

#[cfg(all(target_os = "macos", feature = "packages"))]
pub use pkg_managers::MACOS_PACKAGE_MANAGER_LIST_COMMAND;

#[cfg(all(target_os = "macos", feature = "device"))]
pub use macos_device::get as get_macos_device_name;

#[cfg(feature = "mounts")]
pub(crate) use filesystem_types::FILESYSTEM_TYPES;
