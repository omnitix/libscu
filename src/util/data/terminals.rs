#![cfg(feature = "terminal")]

#[cfg(target_os = "linux")]
pub const LINUX_KNOWN_TERMINALS: [(&str, &str); 14] = [
    ("alacritty", "Alacritty"),
    ("code", "VSCode Terminal"),
    ("codium", "VSCodium Terminal"),
    ("crosh", "ChromeOS developer shell"),
    ("deepin-terminal", "Deepin Terminal"),
    ("foot", "Foot"),
    ("gnome-terminal-", "GNOME Terminal"),
    ("kgx", "GNOME Console"),
    ("kitty", "Kitty"),
    ("konsole", "Konsole"),
    ("lxterminal", "LXTerminal"),
    ("st", "ST"),
    ("xfce4-terminal", "XFCE4 Terminal"),
    ("zed-editor", "Zed Terminal"),
];

#[cfg(target_os = "macos")]
pub const MACOS_KNOWN_TERMINALS: [(&str, &str); 5] = [
    ("Apple_Terminal", "Apple Terminal"),
    ("Hyper", "HyperTerm"),
    ("iTerm.app", "iTerm2"),
    ("Terminal.app", "Apple Terminal"),
    ("vscode", "VSCode terminal"),
];
