#![cfg(feature = "graphics")]

#[derive(Clone, Debug, PartialEq)]
pub enum DesktopEnvironment {
    Aqua,
    Cinnamon,
    GNOME,
    KDEPlasma,
    SurfaceFlinger,
    XFCE4,
}

impl DesktopEnvironment {
    pub fn to_str(&self) -> &'static str {
        match self {
            Self::Aqua => "Aqua",
            Self::GNOME => "GNOME",
            Self::KDEPlasma => "KDE Plasma",
            Self::XFCE4 => "XFCE4",
            Self::Cinnamon => "Cinnamon",
            Self::SurfaceFlinger => "SurfaceFlinger",
        }
    }
}

// binary file name, name
pub const KNOWN_DESKTOP_ENVIRONMENTS: [(&str, DesktopEnvironment); 4] = [
    ("gnome-shell", DesktopEnvironment::GNOME),
    ("plasmashell", DesktopEnvironment::KDEPlasma),
    ("xfce4-session", DesktopEnvironment::XFCE4),
    ("cinnamon-session", DesktopEnvironment::Cinnamon),
];
