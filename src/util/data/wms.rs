#![cfg(feature = "graphics")]

#[derive(Clone, Debug, PartialEq)]
pub enum WindowManager {
    DWM,
    Hyprland,
    #[allow(non_camel_case_types)]
    i3,
    KWin,
    Mutter,
    Niri,
    Openbox,
    Sway,
    XFWM4,
    // #[cfg(target_os = "android")]
    WindowManager,
    // #[cfg(target_os = "macos")]
    QuartzCompositor,
}

impl std::fmt::Display for WindowManager {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            fmt,
            "{}",
            match self {
                #[cfg(target_os = "android")]
                Self::WindowManager => "Window Manager".to_string(),
                #[cfg(target_os = "macos")]
                Self::QuartzCompositor => "Quartz Compositor".to_string(),
                another => format!("{another:?}"),
            }
        )
    }
}

pub const KNOWN_WINDOW_MANAGERS: [(&str, WindowManager); 10] = [
    ("dwm", WindowManager::DWM),
    ("Hyprland", WindowManager::Hyprland),
    ("i3", WindowManager::i3),
    ("kwin_wayland", WindowManager::KWin),
    ("kwin_x11", WindowManager::KWin),
    ("mutter-x11-frames", WindowManager::Mutter),
    ("niri", WindowManager::Niri),
    ("openbox", WindowManager::Openbox),
    ("sway", WindowManager::Sway),
    ("xfwm4", WindowManager::XFWM4),
];
