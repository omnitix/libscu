#![cfg(feature = "extract_version")]

fn program_version_arg(program: &str) -> &'static str {
    match program {
        "dwm" => "-v",
        "hyprctl" => "version",
        _ => "--version",
    }
}

fn process_hyprctl_output(output: &str) -> Option<String> {
    Some(
        output
            .split('\n')
            .find(|l| l.starts_with("Tag: "))?
            .to_string(),
    )
}

pub fn extract_version(program: &str) -> Option<String> {
    let output = std::process::Command::new(program)
        .arg(program_version_arg(program))
        .output()
        .ok()?;
    let mut output_str = String::from_utf8(if ["dwm", "ksh"].contains(&program) {
        output.stderr
    } else {
        output.stdout
    })
    .ok()?;

    if program == "hyprctl" {
        output_str = process_hyprctl_output(&output_str)?;
    }

    Some(
        regex_lite::Regex::new(r"(\d+\.?)+")
            .unwrap()
            .find(output_str.as_str())?
            .as_str()
            .to_string(),
    )
}
