#![allow(unused)]

macro_rules! unsupported_os {
    () => {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Unsupported,
            "Operating system is not supported yet.",
        ));
    };
}

macro_rules! not_found {
    ($message:expr) => {
        std::io::Error::new(std::io::ErrorKind::NotFound, $message)
    };
}

macro_rules! invalid_data {
    ($message:expr) => {
        std::io::Error::new(std::io::ErrorKind::InvalidData, $message)
    };
}

pub(crate) use {invalid_data, not_found, unsupported_os};
