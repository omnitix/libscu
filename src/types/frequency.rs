#[derive(Clone, Debug, Default, PartialEq)]
pub struct Frequency {
    pub hz: u64,
    pub mhz: u32,
    pub ghz: f32,
}

impl Frequency {
    pub fn from_hz(_hz: u64) -> Self {
        Self {
            hz: _hz,
            mhz: _hz as u32 / 1000,
            ghz: _hz as f32 / 1000000_f32,
        }
    }

    pub fn from_mhz(_mhz: u32) -> Self {
        Self {
            hz: _mhz as u64 * 1000,
            mhz: _mhz,
            ghz: _mhz as f32 / 1000_f32,
        }
    }
}
