#[derive(Clone, Debug)]
pub struct Size2D {
    pub width: usize,
    pub height: usize,
}

impl Size2D {
    #[allow(dead_code)]
    pub fn new(w: usize, h: usize) -> Self {
        Self {
            width: w,
            height: h,
        }
    }
}
