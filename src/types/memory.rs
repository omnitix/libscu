/// If you need constant floating point arithmetic, you must enable feature `const_arithmetic`
#[derive(Clone, Copy, PartialEq, PartialOrd, Debug, Default)]
pub struct Memory {
    pub bytes: i64,
    pub blocks: i64,
    pub kb: i64,
    pub mb: i64,
    pub gb: f64,
}

#[cfg(not(feature = "const_arithmetic"))]
impl Memory {
    pub fn from_bytes(bytes: i64) -> Self {
        Self {
            bytes,
            blocks: bytes / 512,
            kb: bytes / 1024,
            mb: bytes / 1048576,
            gb: bytes as f64 / 1073741824_f64,
        }
    }
    pub fn from_blocks(blocks: i64) -> Self {
        Self {
            bytes: blocks * 512,
            blocks,
            kb: blocks / 2,
            mb: blocks / 2048,
            gb: blocks as f64 / 2097152_f64,
        }
    }

    pub fn from_kb(kb: i64) -> Self {
        Self {
            bytes: kb * 1024,
            blocks: kb * 2,
            kb,
            mb: kb / 1024,
            gb: kb as f64 / 1048576_f64,
        }
    }

    pub fn from_mb(mb: i64) -> Self {
        Self {
            bytes: mb * 1048576,
            blocks: mb * 2048,
            kb: mb * 1024,
            mb,
            gb: mb as f64 / 1024_f64,
        }
    }

    pub fn from_gb(gb: i64) -> Self {
        Self {
            bytes: gb * 1073741824,
            blocks: gb * 2097152,
            kb: gb * 1048576,
            mb: gb * 1024,
            gb: gb as f64,
        }
    }
}

#[cfg(feature = "const_arithmetic")]
impl Memory {
    pub const fn from_bytes(bytes: i64) -> Self {
        Self {
            bytes,
            blocks: bytes / 512,
            kb: bytes / 1024,
            mb: bytes / 1048576,
            gb: bytes as f64 / 1073741824_f64,
        }
    }
    pub const fn from_blocks(blocks: i64) -> Self {
        Self {
            bytes: blocks * 512,
            blocks,
            kb: blocks / 2,
            mb: blocks / 2048,
            gb: blocks as f64 / 2097152_f64,
        }
    }

    pub const fn from_kb(kb: i64) -> Self {
        Self {
            bytes: kb * 1024,
            blocks: kb * 2,
            kb,
            mb: kb / 1024,
            gb: kb as f64 / 1048576_f64,
        }
    }

    pub const fn from_mb(mb: i64) -> Self {
        Self {
            bytes: mb * 1048576,
            blocks: mb * 2048,
            kb: mb * 1024,
            mb,
            gb: mb as f64 / 1024_f64,
        }
    }

    pub const fn from_gb(gb: i64) -> Self {
        Self {
            bytes: gb * 1073741824,
            blocks: gb * 2097152,
            kb: gb * 1048576,
            mb: gb * 1024,
            gb: gb as f64,
        }
    }
}

impl std::ops::Add for Memory {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            bytes: self.bytes + other.bytes,
            blocks: self.blocks + other.blocks,
            kb: self.kb + other.kb,
            mb: self.mb + other.mb,
            gb: self.gb + other.gb,
        }
    }
}

impl std::ops::AddAssign for Memory {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            bytes: self.bytes + other.bytes,
            blocks: self.blocks + other.blocks,
            kb: self.kb + other.kb,
            mb: self.mb + other.mb,
            gb: self.gb + other.gb,
        };
    }
}
