pub mod hardware;
pub mod software;
pub mod types;
pub mod util;

pub const VERSION: &str = env!("CARGO_PKG_VERSION");
