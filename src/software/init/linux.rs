#![cfg(target_os = "linux")]

use crate::util;

use super::{super::proc, InitSystem};

use std::io::{Error, ErrorKind, Result};
use std::path::{Path, PathBuf};

fn is_service(file: PathBuf) -> bool {
    let extension = file.extension();
    if let Some(extension) = extension {
        if util::fs::is_executable(file.clone())
            || (file.is_file() && ["service", "rc"].contains(&extension.to_str().unwrap()))
        {
            return true;
        }
    }

    false
}

fn count_services(path: PathBuf) -> Option<u16> {
    if let Ok(scandir) = util::fs::scan_dir(path) {
        let count = scandir
            .iter()
            .filter(|s| is_service(s.to_path_buf()))
            .count() as u16;
        return if count == 0 { None } else { Some(count) };
    }

    None
}

fn comm_init_match(cmdline: &str) -> &'static str {
    if Path::new("/run/dinit").exists() {
        "Dinit"
    } else if Path::new("/usr/share/sysvinit/inittab").exists()
        || Path::new("/etc/inittab").exists()
    {
        "SysVinit"
    } else if std::fs::read_link(cmdline.split('\0').next().unwrap_or_default())
        .unwrap_or_default()
        .to_str()
        == Some("openrc-init")
    {
        "OpenRC"
    } else {
        "init"
    }
}

pub fn fetch_info(init_info: &mut InitSystem) -> Result<()> {
    let proc_info = proc::get_info(1);

    if let Ok(proc_info) = proc_info {
        init_info.name = String::from(match proc_info.command.trim() {
            "systemd" => "SystemD",
            "openrc-init" | "init-openrc" => "OpenRC",
            "runit" => "Runit",
            "init" => comm_init_match(&proc_info.cmdline),
            "s6-svscan" => "S6",
            "upstart" => "Upstart",
            _ => "",
        });

        if !init_info.name.is_empty() {
            for services_dir in if init_info.name == "SystemD" {
                ["/lib/systemd/system", "/etc/systemd/system"]
            } else {
                ["/etc/init.d", "/etc/init"]
            } {
                if let Some(cs) = count_services(PathBuf::from(services_dir)) {
                    init_info.number_of_services = Some(cs);
                    break;
                }
            }
            return Ok(());
        }
    }
    Err(Error::from(ErrorKind::Other))
}
