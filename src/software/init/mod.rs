//! Fetch information about init system
//!
//! Feature `init` must be enabled
//!
//! Supported platforms: Linux, MacOS

#![cfg(feature = "init")]

mod linux;
mod macos;

/// Contains minimum information about init system
#[derive(Clone, Debug, Default)]
pub struct InitSystem {
    pub name: String,
    pub number_of_services: Option<u16>,
}

/// Returns [`InitSystem`]
pub fn fetch_info() -> std::io::Result<InitSystem> {
    let mut init_info = InitSystem::default();

    #[cfg(target_os = "linux")]
    linux::fetch_info(&mut init_info)?;
    #[cfg(target_os = "macos")]
    macos::fetch_info(&mut init_info)?;

    #[cfg(not(any(target_os = "linux", target_os = "macos")))]
    crate::util::error::unsupported_os!();

    Ok(init_info)
}
