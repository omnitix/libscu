#![cfg(target_os = "macos")]

use crate::util;

use super::InitSystem;

pub fn fetch_info(init_info: &mut InitSystem) -> std::io::Result<()> {
    init_info.name = "LaunchD".to_string();

    let mut daemons_count: u16 = 0;

    for daemons_dir in ["/System/Library/LaunchDaemons", "/Library/LaunchDaemons"] {
        if let Ok(daemons_list) = util::fs::list_dir_from_str(daemons_dir, true) {
            daemons_count += daemons_list.len() as u16;
        }
    }

    if daemons_count > 0 {
        init_info.number_of_services = Some(daemons_count);
    }

    Ok(())
}
