#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::util::platform::unix::{libc::LC_ALL, setlocale};

pub fn fetch() -> std::io::Result<String> {
    setlocale(LC_ALL, "")
}
