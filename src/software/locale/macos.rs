#![cfg(target_os = "macos")]

use crate::util::platform::unix::{libc::LC_ALL, setlocale};

pub fn fetch() -> std::io::Result<String> {
    setlocale(LC_ALL, "")
}
