#![cfg(feature = "locale")]

mod linux;
mod macos;

use std::io;

/// Returns current system locale
pub fn fetch() -> io::Result<String> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::fetch();
    #[cfg(target_os = "macos")]
    return macos::fetch();

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
