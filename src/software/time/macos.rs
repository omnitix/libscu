#![cfg(target_os = "macos")]

use crate::types::Time;
use crate::util::platform::macos::libc;

use std::io::{Error, ErrorKind, Result};

pub fn fetch_uptime() -> Result<Time> {
    unsafe {
        let uptime_secs = libc::get_uptime_secs();
        if uptime_secs != 0 {
            return Ok(Time::from_seconds(uptime_secs));
        } else {
            return Err(Error::new(ErrorKind::Other, "uptime is zero (wth?)"));
        }
    }
}
