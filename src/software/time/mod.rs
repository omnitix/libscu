//! Fetch OS uptime
//!
//! Feature `time` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "android", target_os = "linux", target_os = "macos"))]
#![cfg(feature = "time")]

mod linux;
mod macos;

use std::io::Result;

/// Returns [`crate::types::Time`]
pub fn fetch_uptime() -> Result<crate::types::Time> {
    #[cfg(any(target_os = "android", target_os = "linux"))]
    return linux::fetch_uptime();
    #[cfg(target_os = "macos")]
    return macos::fetch_uptime();
    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
