#![cfg(any(target_os = "linux", target_os = "android"))]
#![allow(dead_code)]

use crate::types::Time;
use crate::util::platform::linux::sysinfo;

use std::io::Result;

pub fn fetch_uptime() -> Result<Time> {
    sysinfo().map(|si| Time::from_seconds(si.uptime as u64))
}
