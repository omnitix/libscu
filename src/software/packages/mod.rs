//! Fetch information about installed package managers
//! Feature `packages` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(feature = "packages")]

mod linux;
mod macos;

use std::io::Result;

/// Contains basic information about package manager
#[derive(Clone, Debug)]
pub struct PackageManager {
    pub name: &'static str,
    pub number_of_packages: u64,
}

/// Returns [`Vec`]<[`PackageManager`]>
pub fn fetch_all_managers() -> Result<Vec<PackageManager>> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return Ok(linux::fetch_all_managers());
    #[cfg(target_os = "macos")]
    return Ok(macos::fetch_all_managers());

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
