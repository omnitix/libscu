#![cfg(any(target_os = "linux", target_os = "android"))]

use super::PackageManager;

/// Returns vector of [`PackageManager`]s
pub fn fetch_all_managers() -> Vec<PackageManager> {
    let mut result: Vec<PackageManager> = Vec::new();

    for (manager_name, manager_func) in package_managers::MANAGERS {
        if let Some(number_of_packages) = manager_func() {
            result.push(PackageManager {
                name: manager_name,
                number_of_packages,
            })
        }
    }

    result
}

mod package_managers {
    use std::{
        fs::{read_dir, read_to_string},
        path::Path,
    };

    pub type ManagerFunc = fn() -> Option<u64>;
    pub const MANAGERS: [(&str, ManagerFunc); 10] = [
        ("apk", apk),
        ("dpkg", dpkg),
        ("eopkg", eopkg),
        ("flatpak-system", flatpak_system),
        ("flatpak-user", flatpak_user),
        ("pacman", pacman),
        ("portage", portage),
        ("rpm", rpm),
        ("snap", snap),
        ("xbps", xbps),
    ];

    // TODO: flatpak-user
    pub fn apk() -> Option<u64> {
        read_to_string("/lib/apk/db/installed")
            .ok()
            .map(|content| content.lines().filter(|line| line.is_empty()).count() as u64)
    }
    pub fn dpkg() -> Option<u64> {
        read_dir("/var/lib/dpkg/info").ok().map(|rd| {
            rd.flatten()
                .filter(|entr| {
                    entr.file_name()
                        .to_str()
                        .is_some_and(|filename| filename.ends_with(".list"))
                })
                .count() as u64
        })
    }
    pub fn eopkg() -> Option<u64> {
        read_dir("/var/lib/eopkg/package")
            .ok()
            .map(|rd| rd.count() as u64)
    }
    fn flatpak(path: &Path) -> Option<u64> {
        let mut pkgs: u64 = 0;
        let hidden_packages_re = regex_lite::Regex::new(r".*\.(Locale|Debug)").unwrap();
        for db in ["app", "runtime"] {
            if let Some(rd) = read_dir(path.join(db)).ok().map(|rd| {
                rd.flatten().filter(|entry| {
                    !hidden_packages_re.is_match(&entry.file_name().to_string_lossy())
                })
            }) {
                pkgs += rd
                    .filter_map(|package| {
                        read_dir(&package.path().join("x86_64"))
                            .ok()
                            .map(|package_rd| {
                                package_rd.flatten().map(|arch| read_dir(arch.path()))
                            })
                            .map(|rd| rd.count())
                    })
                    .sum::<usize>() as u64;
            }
        }
        if pkgs > 0 {
            Some(pkgs)
        } else {
            None
        }
    }
    pub fn flatpak_system() -> Option<u64> {
        flatpak(&Path::new("/var/lib/flatpak"))
    }
    pub fn flatpak_user() -> Option<u64> {
        let home = std::env::var("HOME").ok()?;

        flatpak(&Path::new(&home).join(".local/share/flatpak/"))
    }
    pub fn pacman() -> Option<u64> {
        read_dir("/var/lib/pacman/local")
            .ok()
            .map(|rd| rd.count().saturating_sub(1) as u64)
    }
    pub fn portage() -> Option<u64> {
        let _read_dir = read_dir("/var/db/pkg").ok()?.flatten();

        Some(
            _read_dir
                .filter_map(|entry| read_dir(entry.path()).ok().map(|rd| rd.count() as u64))
                .sum::<u64>(),
        )
    }
    pub fn rpm() -> Option<u64> {
        let db = sqlite::open("/var/lib/rpm/rpmdb.sqlite").ok()?;
        if let Ok(mut request) = db.prepare("SELECT count(*) FROM Packages") {
            if request.next().is_ok() {
                let read = request.read::<Option<i64>, _>(0);
                if let Ok(Some(count)) = read {
                    if count > 0 {
                        return Some(count as u64);
                    }
                }
            }
        }
        None
    }
    pub fn snap() -> Option<u64> {
        Some(
            read_dir("/var/lib/snapd/snaps")
                .ok()?
                .flatten()
                .filter(|entry| {
                    entry
                        .file_name()
                        .to_str()
                        .is_some_and(|filename| filename.ends_with(".snap"))
                })
                .count() as u64,
        )
    }
    // TODO: REWRITE THIS SHITTY FUNCTION
    pub fn xbps() -> Option<u64> {
        use std::process::{Command, Stdio};

        let newline_char: &&u8 = &&b'\n';
        Command::new("xbps-query")
            .stdout(Stdio::piped())
            .arg("-l")
            .output()
            .ok()
            .map(|output| {
                output
                    .stdout
                    .iter()
                    .filter(|r#char| r#char == newline_char)
                    .count() as u64
            })
    }
}

/*
com.github.Anuken.Mindustry
com.github.tchx84.Flatseal
com.jaquadro.NBTExplorer
io.github.retux_game.retux
io.github.spacingbat3.webcord
net.codelogistics.clicker
org.freedesktop.Platform
org.freedesktop.Platform.Compat.i386
org.freedesktop.Platform.GL.default
org.freedesktop.Platform.GL.nvidia-565-77
org.freedesktop.Platform.GL32.default
org.freedesktop.Platform.GL32.nvidia-565-77
org.freedesktop.Platform.VAAPI.Intel
org.freedesktop.Platform.VAAPI.Intel.i386
org.freedesktop.Platform.ffmpeg-full
org.freedesktop.Platform.ffmpeg_full.i386
org.freedesktop.Platform.openh264
org.gnome.Platform
org.gtk.Gtk3theme.Breeze
org.kde.KStyle.Adwaita
org.kde.Platform
org.kde.WaylandDecoration.QAdwaitaDecorations
org.sqlitebrowser.sqlitebrowser
org.supertuxproject.SuperTux
ru.linux_gaming.PortProton
*/
