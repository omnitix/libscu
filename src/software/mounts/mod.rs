//! Fetch information about mounted filesystems
//!
//! Supported platforms: Linux, Android

#![cfg(feature = "mounts")]

mod linux;
mod macos;

use std::io;

/// Represents filesystem mount parameters
#[derive(Clone, Debug)]
pub enum MountParameter {
    Flag(String),
    Option { opt: String, value: String },
}

/// Contains deserialized info about mount entry
/// gets info from each line from /proc/self/mounts
#[derive(Clone, Debug)]
pub struct MountEntry {
    pub source: String,
    pub dest: String,
    pub fs: String,
    pub params: Vec<MountParameter>,
}

/// Returns [`Vec`]<[`MountEntry`]> \
/// Parses /proc/self/mounts
///
/// MacOS currently not supported
pub fn fetch_all_mounts() -> io::Result<Vec<MountEntry>> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::fetch_all_mounts();

    #[cfg(not(any(target_os = "linux", target_os = "android")))]
    crate::util::error::unsupported_os!();
}

/// Returns fstype of mountpoint
/// * path - mount destination
pub fn get_mountpoint_fstype(path: &str) -> io::Result<String> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::get_mountpoint_fstype(path);
    #[cfg(target_os = "macos")]
    return macos::get_mountpoint_fstype(path);

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
