/* TODO LIST OF MOUNTED FILESYSTEMS
use libc::{c_int, c_void, statfs};
use std::ffi::CStr;
use std::ptr;

const MNT_NOWAIT: c_int = 0;

fn main() {
    let mut mnti: *mut statfs = ptr::null_mut();

    unsafe {
        let mut mntsize = libc::getmntinfo(&mut mnti, MNT_NOWAIT);
        if mntsize <= 0 {
            eprintln!("getmntinfo failed");
            return;
        }

        mnti = libc::malloc(mntsize as usize) as *mut statfs;
        if mnti.is_null() {
            eprintln!("malloc failed");
            return;
        }

        mntsize = libc::getmntinfo(&mut mnti, MNT_NOWAIT);
        if mntsize <= 0 {
            eprintln!("getmntinfo failed");
            return;
        }

        for i in 0..mntsize {
            let f_fstypename = CStr::from_ptr((*mnti.offset(i as isize)).f_fstypename.as_ptr());
            let f_mntonname = CStr::from_ptr((*mnti.offset(i as isize)).f_mntonname.as_ptr());
            println!("{} : {}", f_fstypename.to_str().unwrap(), f_mntonname.to_str().unwrap());
        }

        libc::free(mnti as *mut c_void);
    }
}
*/

#![cfg(target_os = "macos")]

use std::ffi::CStr;

use crate::util::platform::macos;

pub fn get_mountpoint_fstype(path: &str) -> std::io::Result<String> {
    let statfs = macos::statfs(path)?;
    unsafe {
        match CStr::from_ptr(statfs.f_fstypename.as_ptr()).to_str() {
            Ok(string) => Ok(string.to_string()),
            Err(_) => Err(std::io::Error::from(std::io::ErrorKind::InvalidData)),
        }
    }
}
