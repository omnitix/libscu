#![cfg(any(target_os = "linux", target_os = "android"))]

use super::{MountEntry, MountParameter};

use std::{fs, io};

const PROC_MOUNTS: &str = "/proc/self/mounts";

fn parse_param(param: &str) -> Option<MountParameter> {
    if param.contains('=') {
        let mut splitted = param.split('=');
        Some(MountParameter::Option {
            opt: splitted.next()?.into(),
            value: splitted.next()?.into(),
        })
    } else {
        Some(MountParameter::Flag(param.into()))
    }
}

fn parse_params(params: &str) -> Vec<MountParameter> {
    params
        .split(',')
        .filter(|param| !param.is_empty())
        .filter_map(parse_param)
        .collect()
}

fn parse_line(line: &str) -> Option<MountEntry> {
    let mut line = line.split(' ');
    Some(MountEntry {
        source: line.next()?.into(),
        dest: line.next()?.into(),
        fs: line.next()?.into(),
        params: parse_params(line.next()?),
    })
}

pub(crate) fn fetch_all_mounts() -> io::Result<Vec<MountEntry>> {
    Ok(fs::read_to_string(PROC_MOUNTS)?
        .split('\n')
        .filter(|l| !l.is_empty())
        .filter_map(parse_line)
        .collect())
}

pub fn get_mountpoint_fstype(path: &str) -> io::Result<String> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    {
        use crate::util::platform::linux::statfs;
        let f_type = statfs(path)?.f_type;
        let fstype = f_type_to_string(f_type);
        match fstype {
            Some(fstype) => Ok(fstype.to_string()),
            None => Err(io::Error::from(io::ErrorKind::NotFound)),
        }
    }

    #[cfg(not(any(target_os = "linux", target_os = "android")))]
    crate::util::error::unsupported_os!();
}

pub fn f_type_to_string(f_type: i64) -> Option<&'static str> {
    use crate::util::data::FILESYSTEM_TYPES;

    FILESYSTEM_TYPES
        .iter()
        .find(|(type_magic, _)| type_magic == &f_type)
        .map(|(_, type_string)| *type_string)
}
