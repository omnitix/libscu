#![cfg(target_os = "macos")]

use crate::util::platform::macos::libc;

use super::User;

use std::io::{Error, ErrorKind, Result};

fn cstring_to_string(raw_pointer: *const i8) -> Result<String> {
    unsafe {
        if raw_pointer.is_null() {
            return Err(Error::new(ErrorKind::InvalidData, "raw_pointer is nullptr"));
        } else {
            Ok(std::ffi::CStr::from_ptr(raw_pointer)
                .to_string_lossy()
                .into_owned())
        }
    }
}

pub fn fetch_user(uid: i32) -> Result<User> {
    unsafe {
        let pwd = libc::getpwuid(uid);

        if pwd.is_null() {
            return Err(Error::from(ErrorKind::NotFound));
        }

        let username = cstring_to_string((*pwd).pw_name)?;
        let home_dir = cstring_to_string(libc::get_pwd_homedir());

        Ok(User {
            name: username,
            uid: (*pwd).pw_uid as u32,
            gid: (*pwd).pw_gid as u32,
            home_dir: home_dir.ok(),
            shell: cstring_to_string(libc::get_pwd_shell()).ok(),
        })
    }
}

pub fn fetch_current() -> Result<User> {
    unsafe { fetch_user(libc::getuid() as i32) }
}

pub fn is_root() -> bool {
    unsafe { libc::getuid() == 0 }
}
