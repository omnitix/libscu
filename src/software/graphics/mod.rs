//! Fetch information about desktop graphics running
//!
//! Features `graphics` must be enabled \
//! Feature `extract_version` is optional, used for fetching version
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(feature = "graphics")]
#![allow(unreachable_code)]

mod android;
mod linux;
mod macos;

use std::io::Result;

/// Contains Wayland and X11
#[derive(Clone, Debug)]
pub enum DisplayServer {
    Wayland,
    X11,
}

/// Returns [`DisplayServer`]
///
/// This is a linux-specific function \
/// Returns None if the OS is not supported or display server could not be obtained
pub fn fetch_display_server() -> Result<DisplayServer> {
    #[cfg(target_os = "linux")]
    return linux::fetch_display_server();

    #[cfg(not(target_os = "linux"))]
    crate::util::error::unsupported_os!();
}

use crate::util::data::{DesktopEnvironment, WindowManager as WM};

/// Returns environment name (such as GNOME or KDE Plasma, look in sources)
pub fn fetch_environment() -> Result<DesktopEnvironment> {
    #[cfg(target_os = "android")]
    return Ok(android::fetch_environment());
    #[cfg(target_os = "linux")]
    return linux::fetch_environment();
    #[cfg(target_os = "macos")]
    return Ok(macos::fetch_environment());

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}

/// Contains basic information about window manager
#[derive(Clone, Debug)]
pub struct WindowManager {
    pub name: Option<WM>,
    pub version: Option<String>,
}

/// Returns [`WindowManager`]
///
/// enable_version enables version fetching
pub fn fetch_window_manager(_enable_version: bool) -> Result<WindowManager> {
    #[cfg(target_os = "android")]
    return Ok(android::fetch_window_manager());
    #[cfg(target_os = "linux")]
    return linux::fetch_window_manager(_enable_version);
    #[cfg(target_os = "macos")]
    return Ok(macos::fetch_window_manager());

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
