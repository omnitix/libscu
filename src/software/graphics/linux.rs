#![cfg(target_os = "linux")]

#[cfg(feature = "extract_version")]
use crate::util::extract_version;
use crate::{
    software::proc,
    util::{
        data::{DesktopEnvironment, KNOWN_DESKTOP_ENVIRONMENTS, KNOWN_WINDOW_MANAGERS},
        string::truncate,
    },
};

use super::{DisplayServer, WindowManager};

use std::io::{Error, ErrorKind, Result};

/// Returns [`DisplayServer`]
///
/// Linux only
pub fn fetch_display_server() -> Result<DisplayServer> {
    Ok(
        match std::env::var("XDG_SESSION_TYPE")
            .map_err(|err| {
                Error::from(match err {
                    std::env::VarError::NotPresent => ErrorKind::NotFound,
                    std::env::VarError::NotUnicode(_) => ErrorKind::InvalidData,
                })
            })?
            .as_str()
        {
            "wayland" => DisplayServer::Wayland,
            "x11" => DisplayServer::X11,
            another => {
                return Err(Error::new(
                    ErrorKind::NotFound,
                    format!("unknown display server: {another}"),
                ))
            }
        },
    )
}

pub fn fetch_environment() -> Result<DesktopEnvironment> {
    for process in proc::list_pids()? {
        for de in KNOWN_DESKTOP_ENVIRONMENTS {
            if let Ok(process) = proc::get_info(process) {
                if process.command.as_str() == truncate(de.0, 15) {
                    return Ok(de.1);
                }
            }
        }
    }

    Err(Error::from(ErrorKind::NotFound))
}

pub fn fetch_window_manager(_enable_version: bool) -> Result<WindowManager> {
    let mut result = WindowManager {
        name: None,
        version: None,
    };

    for process in proc::list_pids()? {
        for wm in KNOWN_WINDOW_MANAGERS {
            if let Ok(process) = proc::get_info(process) {
                if process.command.as_str() == truncate(wm.0, 15) {
                    result.name = Some(wm.1);

                    #[cfg(feature = "extract_version")]
                    if _enable_version {
                        let wm = result.name.clone().unwrap().to_string();
                        result.version = extract_version(match wm.as_str() {
                            "Mutter" => "mutter",
                            "Hyprland" => "hyprctl",
                            _ => process.command.as_str(),
                        });
                    }
                    return Ok(result);
                }
            }
        }
    }

    Err(Error::from(ErrorKind::NotFound))
}
