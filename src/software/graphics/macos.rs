#![cfg(target_os = "macos")]

use super::WindowManager;
use crate::util::data::{DesktopEnvironment, WindowManager as WM};

pub fn fetch_environment() -> DesktopEnvironment {
    DesktopEnvironment::Aqua
}

pub fn fetch_window_manager() -> WindowManager {
    WindowManager {
        name: Some(WM::QuartzCompositor),
        version: None,
    }
}
