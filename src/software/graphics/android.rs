#![cfg(target_os = "android")]

use super::WindowManager;
use crate::util::data::{DesktopEnvironment, WindowManager as WM};

pub fn fetch_environment() -> DesktopEnvironment {
    DesktopEnvironment::SurfaceFlinger
}

pub fn fetch_window_manager() -> WindowManager {
    WindowManager {
        name: Some(WM::WindowManager),
        version: None,
    }
}
