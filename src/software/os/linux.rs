#![cfg(target_os = "linux")]

use std::{fs, io};

use super::OSRelease;

pub fn fetch_os_release(osrelease: &mut OSRelease) -> io::Result<()> {
    for line in fs::read_to_string("/etc/os-release")?.split('\n') {
        if let Some((var, val)) = line.split_once('=') {
            let (var, val) = (var.trim(), val.replace("\"", "").trim().to_string());
            match var {
                "NAME" => osrelease.name = val,
                "VERSION" => osrelease.version = Some(val),
                "ID" => osrelease.id = val,
                "VERSION_ID" => osrelease.version_id = Some(val),
                "BUILD_ID" => osrelease.build_id = Some(val),
                "PRETTY_NAME" => osrelease.pretty_name = val,
                "ANSI_COLOR" => osrelease.ansi_color = Some(val),
                "HOME_URL" => osrelease.home_url = val,
                "SUPPORT_URL" => osrelease.support_url = Some(val),
                "BUG_REPORT_URL" => osrelease.bug_report_url = Some(val),
                "PRIVACY_POLICY_URL" => osrelease.privacy_policy_url = Some(val),
                _ => {}
            }
        }
    }

    Ok(())
}
