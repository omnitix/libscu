#![cfg(target_os = "android")]

use crate::util::platform::android;

use super::OSRelease;

pub fn fetch_os_release(osrelease: &mut OSRelease) {
    let version = android::getprop("ro.build.version.release").unwrap_or_default();
    osrelease.name = "Android".to_string();
    osrelease.pretty_name = format!("{} {version}", &osrelease.name).trim().to_string();
    osrelease.id = "android".to_string();
    osrelease.version = Some(version);
    osrelease.home_url = "https://www.android.com/".to_string();
    osrelease.support_url = Some("https://support.google.com/android".to_string());
}
