//! Fetch OS name
//!
//! Feature `os` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "os")]

mod android;
mod linux;
mod macos;

use std::io;

/// Contains name of an operating system
#[derive(Clone, Debug, Default)]
pub struct OSRelease {
    pub name: String,
    pub version: Option<String>,
    pub id: String,
    pub version_id: Option<String>,
    pub build_id: Option<String>,
    pub pretty_name: String,
    pub ansi_color: Option<String>,
    pub home_url: String,
    pub support_url: Option<String>,
    pub bug_report_url: Option<String>,
    pub privacy_policy_url: Option<String>,
}

/// Returns [`OSRelease`]
pub fn fetch_release() -> io::Result<OSRelease> {
    let mut result = OSRelease::default();

    #[cfg(target_os = "android")]
    android::fetch_os_release(&mut result);
    #[cfg(target_os = "linux")]
    linux::fetch_os_release(&mut result)?;
    #[cfg(target_os = "macos")]
    let _ = macos::fetch_os_release(&mut result);

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    return None;

    (result.name, result.pretty_name) = (
        result.name.trim().to_string(),
        result.pretty_name.trim().to_string(),
    );

    if result.name.is_empty() {
        return Err(io::Error::new(
            io::ErrorKind::NotFound,
            "NAME not found or empty",
        ));
    }

    Ok(result)
}
