//! Fetch information about shell
//!
//! Feature `shell` must be enabled \
//! Feature `extract_version` is optional, used for fetching version
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(feature = "shell")]

use crate::util::data;
#[cfg(feature = "extract_version")]
use crate::util::extract_version;

use super::proc;

use std::io::{Error, ErrorKind, Result};

/// Contains basic information about shell interpreter
#[derive(Clone, Debug)]
pub struct Shell {
    pub name: String,
    pub version: Option<String>,
}

/// Returns [`Shell`]
pub fn fetch_info(enable_version: bool) -> Result<Shell> {
    #[cfg(any(target_os = "android", target_os = "linux", target_os = "macos"))]
    {
        let mut ppid = proc::get_ppid(proc::get_self_pid()).unwrap_or(1);
        while ppid > 1 {
            let command = proc::get_info(ppid).unwrap().command;
            if data::KNOWN_SHELLS.contains(&format!("/bin/{}", command).as_str()) {
                let mut result = Shell {
                    name: command.clone(),
                    version: None,
                };
                #[cfg(feature = "extract_version")]
                if enable_version && command != "dash" {
                    result.version = extract_version(command.as_str());
                }
                return Ok(result);
            }
            ppid = proc::get_ppid(ppid).unwrap();
        }
        Err(Error::from(ErrorKind::NotFound))
    }
    #[cfg(not(any(target_os = "android", target_os = "linux", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
