#![cfg(any(target_os = "linux", target_os = "android"))]

use super::Process;

use std::fs;
use std::io::{Error, ErrorKind, Result};

pub fn get_ppid(pid: u32) -> Result<u32> {
    let proc_status_content = fs::read_to_string(format!("/proc/{pid}/status"))?;
    for line in proc_status_content.split('\n') {
        let (var, val) = line.split_once(":").unwrap_or_default();
        let (var, val) = (var.trim(), val.trim());
        if var == "PPid" {
            if let Ok(ppid) = val.parse::<u32>() {
                return Ok(ppid);
            }
            break;
        }
    }

    Err(Error::new(
        ErrorKind::NotFound,
        format!("Parent PID not found for PID {pid}"),
    ))
}

pub fn get_info(process: &mut Process, pid: u32) -> Result<()> {
    process.cmdline = fs::read_to_string(format!("/proc/{}/cmdline", pid))?
        .trim()
        .to_string();
    process.command = fs::read_to_string(format!("/proc/{}/comm", pid))?
        .trim()
        .to_string();

    Ok(())
}

pub fn list_pids() -> Result<Vec<u32>> {
    Ok(std::fs::read_dir("/proc")?
        .filter_map(|v| v.unwrap().file_name().to_str().unwrap().parse::<u32>().ok())
        .collect())
}
