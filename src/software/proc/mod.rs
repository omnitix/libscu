//! Fetch information about running processes
//!
//! Feature `proc` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "proc")]

mod linux;
mod macos;

use std::io::Result;

use std::process;

/// Contains basic information about process
#[derive(Clone, Debug, Default)]
pub struct Process {
    pub pid: u32, // process id
    pub cmdline: String,
    pub command: String,
}

/// Returns id of current process
pub fn get_self_pid() -> u32 {
    process::id()
}

/// Returns parent process id
pub fn get_ppid(pid: u32) -> Result<u32> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::get_ppid(pid);
    #[cfg(target_os = "macos")]
    return macos::get_ppid(pid);

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}

/// Returns [`Process`]
///
/// On MacOS [`Process::cmdline`] field equals [`Process::command`]
pub fn get_info(pid: u32) -> Result<Process> {
    let mut result = Process {
        pid,
        ..Default::default()
    };

    #[cfg(any(target_os = "linux", target_os = "android"))]
    linux::get_info(&mut result, pid)?;
    #[cfg(target_os = "macos")]
    macos::get_info(&mut result, pid)?;
    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();

    Ok(result)
}

/// Returns vector of [`Process`] (lists all available processes)
pub fn list_pids() -> Result<Vec<u32>> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::list_pids();
    #[cfg(target_os = "macos")]
    return macos::list_pids();

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
