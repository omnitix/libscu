#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::{
    software::proc,
    types::Size2D,
    util::platform::unix::libc::{ioctl, WinSize, STDIN_FILENO, TIOCGWINSZ},
};

#[cfg(target_os = "linux")]
use crate::util::data::LINUX_KNOWN_TERMINALS;

use super::TerminalInfo;

#[cfg(target_os = "linux")]
fn bin_to_name(bin_name: &str) -> Option<String> {
    LINUX_KNOWN_TERMINALS
        .iter()
        .find(|term| term.0 == bin_name)
        .map(|term| term.1.to_string())
}

fn fetch_size() -> Option<Size2D> {
    let mut nix_size = WinSize {
        ws_row: 0,
        ws_col: 0,
        ws_xpixel: 0,
        ws_ypixel: 0,
    };

    if unsafe { ioctl(STDIN_FILENO, TIOCGWINSZ, &mut nix_size) } == 0 {
        return Some(Size2D {
            width: nix_size.ws_col as usize,
            height: nix_size.ws_row as usize,
        });
    }

    None
}

pub fn fetch_info(result_buf: &mut TerminalInfo, _fetch_version: bool) -> std::io::Result<()> {
    result_buf.name = "Linux".to_string();

    #[cfg(target_os = "android")]
    if std::path::Path::new("/data/data/com.termux/files/home/.termux").exists() {
        result_buf.name = "Termux".to_string();
        if _fetch_version {
            result_buf.version = std::env::var("TERMUX_VERSION").ok()
        }
    }
    #[cfg(target_os = "linux")]
    {
        // still doesn't work from tmux
        let mut ppid = proc::get_ppid(proc::get_self_pid())?;
        while ppid != 1 {
            let proc_info = proc::get_info(ppid)?;
            if proc_info.command == "ld-linux-x86-64"
                && proc_info.cmdline.contains("cros-containers")
            {
                result_buf.name = "Crostini LXC container".to_string();
                break;
            } else if let Some(got_name) = bin_to_name(&proc_info.command) {
                result_buf.name = got_name;
                break;
            }
            ppid = proc::get_ppid(ppid)?;
        }
    }

    result_buf.size = fetch_size();

    Ok(())
}
