#![cfg(target_os = "macos")]

use crate::{
    types::Size2D,
    util::{data::MACOS_KNOWN_TERMINALS, platform::macos::libc},
};

use super::TerminalInfo;

use std::io::{Error, ErrorKind, Result};

fn fetch_size() -> Option<Size2D> {
    unsafe {
        let (mut cols, mut rows): (i32, i32) = (0, 0);

        if libc::get_terminal_size(&mut cols, &mut rows) == 0 {
            Some(Size2D {
                width: cols as usize,
                height: rows as usize,
            })
        } else {
            None
        }
    }
}

fn beautify_terminal_name(name: &str) -> Option<String> {
    MACOS_KNOWN_TERMINALS
        .iter()
        .find(|term| term.0 == name)
        .and_then(|term| Some(term.1.to_string()))
}

pub fn fetch_info(result_buf: &mut TerminalInfo) -> Result<()> {
    let env_var = std::env::var("TERM_PROGRAM");

    match env_var {
        Ok(var) => {
            result_buf.name = if let Some(term_name) = beautify_terminal_name(var.trim()) {
                term_name
            } else {
                if var.is_empty() {
                    return Err(Error::new(
                        ErrorKind::Other,
                        "Environment variable $TERM_PROGRAM is empty",
                    ));
                } else {
                    var.replace(".app", "")
                }
            };
        }
        Err(_) => {
            return Err(Error::new(
                ErrorKind::Other,
                "Failed to get $TERM_PROGRAM environment variable",
            ))
        }
    }

    result_buf.size = fetch_size();

    Ok(())
}
