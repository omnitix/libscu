//! Fetch hostname
//!
//! Feature `hostname` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(feature = "hostname")]

use crate::util::platform::unix::gethostname;

#[cfg(target_os = "macos")]
fn process(hostname: String, _raw: bool) -> String {
    hostname.replace(
        if cfg!(target_os = "macos") && _raw {
            ""
        } else {
            ".local"
        },
        "",
    )
}

/// Returns HOSTNAME
///
/// MacOS-specific:
/// * raw arg - remove '.local' from result
pub fn fetch(#[cfg(target_os = "macos")] raw: bool) -> std::io::Result<String> {
    #[cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
    {
        let hostname = gethostname()?;

        #[cfg(target_os = "macos")]
        return Ok(process(hostname, raw));
        #[cfg(not(target_os = "macos"))]
        return Ok(hostname);
    }

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
