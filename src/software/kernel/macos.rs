#![cfg(target_os = "macos")]

use crate::util::platform::macos::{uname_kernel_name, uname_kernel_release, uname_machine_arch};

use std::io::Result;

pub fn fetch_name() -> Result<String> {
    uname_kernel_name()
}

pub fn fetch_version() -> Result<String> {
    uname_kernel_release()
}

pub fn fetch_arch() -> Result<String> {
    uname_machine_arch()
}
