#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::util::platform::linux::{uname_kernel_name, uname_kernel_release, uname_machine_arch};

use std::io::Result;

pub fn fetch_name() -> Result<String> {
    uname_kernel_name()
}

pub fn fetch_version() -> Result<String> {
    uname_kernel_release()
}

pub fn fetch_arch() -> Result<String> {
    uname_machine_arch()
}
