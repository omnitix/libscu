//! Fetch kernel version and arch (arch is Linux-only)
//!
//! Feature `kernel` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(feature = "kernel")]

mod linux;
mod macos;

use std::io::Result;

/// Returns kernel's name
pub fn fetch_name() -> Result<String> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::fetch_name();
    #[cfg(target_os = "macos")]
    return macos::fetch_name();

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}

/// Returns kernel's version
pub fn fetch_version() -> Result<String> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::fetch_version();
    #[cfg(target_os = "macos")]
    return macos::fetch_version();

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}

/// Returns kernel's arch
pub fn fetch_arch() -> Result<String> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::fetch_arch();
    #[cfg(target_os = "macos")]
    return macos::fetch_arch();

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
