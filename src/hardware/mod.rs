pub mod battery;
pub mod bootmode;
pub mod cpu;
pub mod device;
pub mod disk;
pub mod display;
pub mod gpu;
pub mod ram;
