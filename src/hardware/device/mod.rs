//! Fetch device name
//!
//! Feature `device` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(feature = "device")]

use crate::util;

fn beautify(model: &str) -> String {
    let mut result = String::from(model);

    // remove trash
    for trash in [
        "System Product Name",
        "System Version",
        "To Be Filled By O.E.M.",
        "To be filled by O.E.M.",
        "Default string",
        "Type1ProductConfigId",
        "Technology Co., Ltd.",
    ] {
        result = result.replace(trash, "");
    }

    result = result
        .replace("ASUSTeK COMPUTER INC.", "ASUS") // make ASUS brandname shorter
        .replace("Hewlett-Packard", "HP"); // make HP brandname shorter

    util::string::remove_multiple_spaces(result.replace(['\0', '\n'], "").trim())
}

mod android;
mod linux;
mod macos;

use std::io;

/// Returns device name (model or board name, depends on your luck :D)
///
/// on MacOS returns device model correctly
///
/// * raw arg - if false then trash like "To be filled by O.E.M." or "Default string" will be removed from result
pub fn fetch_model(raw: bool) -> io::Result<String> {
    let mut result = String::new();

    #[cfg(target_os = "android")]
    android::fetch_model(raw, &mut result);
    #[cfg(target_os = "linux")]
    linux::fetch_model(&mut result);
    #[cfg(target_os = "macos")]
    macos::fetch_model(&mut result)?;

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();

    if result.is_empty() {
        return Err(io::Error::from(io::ErrorKind::NotFound));
    }

    if !raw {
        result = beautify(&result);
    }

    // remove repeating words
    result = util::string::uniqalize(&result);

    Ok(result.trim().to_string())
}
