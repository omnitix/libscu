#![cfg(target_os = "macos")]

use crate::util::data::get_macos_device_name;

use sysctl::{Ctl, Sysctl};

use std::io::{Error, ErrorKind, Result};

pub fn fetch_model(model_buf: &mut String) -> Result<()> {
    *model_buf = Ctl::new("hw.model")
        .map_err(|error| Error::new(ErrorKind::Other, error.to_string()))?
        .value_string()
        .map_err(|error| Error::new(ErrorKind::Other, error.to_string()))?;

    if let Some(identified_model) = get_macos_device_name(&model_buf) {
        model_buf.clear();
        model_buf.push_str(&identified_model);
    }
    Ok(())
}
