//! Fetch brightness level on display
//!
//! Feature `display` must be enabled
//!
//! Supported platforms: Android, Linux

#![cfg(feature = "display")]

mod android;
mod linux;

use std::io;

/// Contains max and current brightness
#[derive(Clone, Debug, Default)]
pub struct Brightness {
    pub max: u32,
    pub current: u32,
}

/// Returns Brightness structure \
/// returns None if platform is unsupported or brightness can't be fetched
pub fn fetch_brightness() -> io::Result<Brightness> {
    #[cfg(target_os = "android")]
    return android::brightness();
    #[cfg(target_os = "linux")]
    return linux::brightness();

    #[cfg(not(any(target_os = "linux", target_os = "android")))]
    crate::util::error::unsupported_os!();
}
