#![cfg(target_os = "android")]

use super::Brightness;
use crate::util::platform::android;

use std::io;

pub fn brightness() -> io::Result<Brightness> {
    let prop = android::getprop("debug.tracing.screen_brightness")?;
    let brightness = prop.parse::<f32>().map_err(|_| {
        io::Error::new(
            io::ErrorKind::InvalidData,
            "invalid debug.tracing.screen_brightness: {prop}",
        )
    })?;

    return Ok(Brightness {
        max: 100,
        current: (brightness * 100.0) as u32,
    });
}
