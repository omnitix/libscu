#![cfg(target_os = "linux")]

use super::Brightness;

use crate::util::fs;

use std::io;

pub fn brightness() -> io::Result<Brightness> {
    let dir_content = fs::list_dir_from_str("/sys/class/backlight", true)?;
    for backlight_data in dir_content {
        let (max, current) = (
            fs::get_prop(&backlight_data, "max_brightness")
                .unwrap_or_default()
                .parse::<u32>()
                .unwrap_or(0),
            fs::get_prop(&backlight_data, "brightness")
                .unwrap_or_default()
                .parse::<u32>()
                .unwrap_or(0),
        );
        if current > 0 && max > 0 {
            return Ok(Brightness { max, current });
        }
    }

    Err(io::Error::from(io::ErrorKind::NotFound))
}
