#![cfg(target_os = "linux")]

use crate::{types, util};

use std::{
    fs,
    io::{self, Read, Result, Seek, SeekFrom},
    path::{Path, PathBuf},
};

use super::*;

const SYSFS_BLOCKS: &str = "/sys/block";

#[derive(Clone, Debug)]
pub struct UEVENT {
    pub dev_name: String,
    pub dev_type: String,
}

impl UEVENT {
    fn parse(path: PathBuf) -> Option<Self> {
        let mut uevent = UEVENT {
            dev_name: String::new(),
            dev_type: String::new(),
        };

        if let Ok(uevent_read) = fs::read_to_string(path) {
            for line in uevent_read.split("\n").filter(|l| !l.is_empty()) {
                let (var, val) = line.trim().split_once("=").unwrap_or(("", ""));
                match var {
                    "DEVNAME" => uevent.dev_name = val.to_string(),
                    "DEVTYPE" => uevent.dev_type = val.to_string(),
                    _ => {}
                }
            }
        }

        if uevent.dev_name.is_empty() || uevent.dev_type.is_empty() {
            None
        } else {
            Some(uevent)
        }
    }
}

impl Partition {
    fn is_partition_dir(path: &Path) -> bool {
        path.join("size").is_file()
            && path.join("uevent").is_file()
            && path.join("partition").is_file()
    }
    pub fn fetch_all(disk: &mut super::Disk) -> Result<()> {
        let disk_dev_name = match disk.dev_path.file_name() {
            Some(dev_name) => dev_name,
            None => {
                return Err(Error::new(
                    ErrorKind::InvalidData,
                    "Disk does not contain a dev_path (damaged?)",
                ))
            }
        };
        let dir_list = util::fs::list_dir(PathBuf::from("/sys/block").join(disk_dev_name), true)?;
        for partition_dir in dir_list
            .into_iter()
            .filter(|entry| Self::is_partition_dir(entry))
        {
            let mut partition = Partition {
                dev_path: PathBuf::from("/dev"),
                disk_dev_path: disk.dev_path.clone(),
                number: 0,
                size: Memory::default(),
                readonly: false,
            };

            if !UEVENT::parse(partition_dir.join("uevent")).is_some_and(|uevent| {
                if !uevent.dev_name.is_empty() {
                    partition.dev_path = partition.dev_path.join(uevent.dev_name);
                    return true;
                }
                false
            }) || !util::fs::get_prop(&partition_dir, "size").is_ok_and(|size| {
                size.parse::<i64>().is_ok_and(|size| {
                    partition.size = Memory::from_blocks(size);
                    true
                })
            }) || !util::fs::get_prop(&partition_dir, "ro").is_ok_and(|ro| {
                partition.readonly = ro.contains("1");
                true
            }) || !util::fs::get_prop(&partition_dir, "partition").is_ok_and(|part_n| {
                part_n.parse::<u8>().is_ok_and(|part_n| {
                    partition.number = part_n;
                    true
                })
            }) {
                continue;
            }

            disk.partitions.push(partition);
        }

        Ok(())
    }
}

pub mod disk {
    use super::*;
    fn is_virtual_disk(fname: &str) -> bool {
        fname.is_empty()
            || fname.starts_with("dm")
            || fname.starts_with("loop")
            || fname.starts_with("sr")
            || fname.starts_with("zram")
            || fname.starts_with("ram")
            || fname.contains("boot")
            || fname.starts_with("mtdblock")
            || fname.starts_with("block")
    }
    fn is_correct_disk(uevent: &UEVENT) -> bool {
        !is_virtual_disk(uevent.dev_name.as_str())
            && uevent.dev_type == "disk"
            && std::path::Path::new("/dev").join(&uevent.dev_name).exists()
    }
    pub fn technology(dev: &Path) -> DiskTechnology {
        if super::util::fs::get_prop(dev, "queue/rotational")
            .unwrap_or("".to_string())
            .contains("1")
        {
            DiskTechnology::HDD
        } else {
            DiskTechnology::SSD
        }
    }
    pub fn fetch_disk_info<T>(device: T) -> io::Result<Disk>
    where
        T: ToString,
    {
        let mut device = device.to_string();
        if device.starts_with("/dev/") && std::path::Path::new(&device).exists() {
            device = device.replace("/dev/", "");
        }
        let mut result = Disk::default();

        let dev_path = PathBuf::from(SYSFS_BLOCKS).join(&device);

        result.technology = technology(&dev_path);
        result.removable =
            util::fs::get_prop(&dev_path, "removable").unwrap_or("".to_string()) == "1";

        for model_name_file in ["model", "name"] {
            if let Ok(m) = util::fs::get_prop(&dev_path.join("device"), model_name_file) {
                result.model = Some(m);
                break;
            }
        }

        result.size = types::Memory::from_blocks(
            util::fs::get_prop(&dev_path, "size")
                .unwrap_or("".to_string())
                .parse::<i64>()
                .unwrap_or(0),
        );
        if result.size.blocks == 0 {
            return Err(Error::new(
                ErrorKind::NotFound,
                format!("/dev/{device} has zero size"),
            ));
        }

        result.dev_path = PathBuf::from("/dev").join(&device);
        if !result.dev_path.exists() {
            return Err(Error::new(
                ErrorKind::NotFound,
                format!("/dev/{device} not found"),
            ));
        }

        Ok(result)
    }
    pub fn partition_table_type(disk: &mut super::Disk) -> Result<()> {
        let mut disk_reader = std::fs::File::open(&disk.dev_path)?;

        let _ = disk_reader.seek(SeekFrom::Start(0));
        let mut bytes = [0u8; 520];
        disk_reader.read_exact(&mut bytes)?;

        if bytes[512..520] == GPT_SIGNATURE {
            disk.partition_table_type = Some(PartitionTableType::GPT);
            Ok(())
        } else if bytes[510..512] == [0x55, 0xaa] {
            disk.partition_table_type = Some(PartitionTableType::MBR);
            Ok(())
        } else {
            disk.partition_table_type = None;
            Err(Error::new(
                ErrorKind::NotFound,
                "Partition table not recognized.",
            ))
        }
    }
    pub fn identify_filesystems(disk: &mut super::Disk) -> Result<Option<Filesystem>> {
        if disk.partitions.is_empty() {
            return Err(Error::new(
                ErrorKind::NotFound,
                "Disk contains no partitions.",
            ));
        }

        match disk.partition_table_type {
            Some(PartitionTableType::MBR) => {
                let mut disk_read = std::fs::OpenOptions::new()
                    .read(true)
                    .write(false)
                    .append(false)
                    .open(&disk.dev_path)?;

                let mut disk_metadata: [u8; 512] = [0; 512];
                disk_read.read_exact(&mut disk_metadata)?;

                for partition in &disk.partitions {
                    let pos = 446 + 0x04 + (16 * (partition.number as usize - 1));
                    let fs_byte = disk_metadata[pos];
                    println!(
                        "partition #{} fs at pos ({}): {:?} byte ({})",
                        partition.number,
                        pos,
                        Filesystem::from_mbr_byte(fs_byte),
                        fs_byte
                    );
                }
            }
            _ => {
                return Err(Error::new(
                    ErrorKind::NotFound,
                    "Disk does not contain any known partitions table",
                ))
            }
        }

        Ok(None)
    }
    pub fn fetch_disks() -> io::Result<Vec<Disk>> {
        let mut result = Vec::<Disk>::new();

        for device in fetch_devices()? {
            if let Ok(device) = fetch_disk_info(device) {
                result.push(device);
            }
        }

        Ok(result)
    }
    pub fn fetch_devices() -> io::Result<Vec<String>> {
        let mut detected_devices = Vec::<String>::new();

        let sysfs_block_path = PathBuf::from(SYSFS_BLOCKS);

        for device in fs::read_dir(sysfs_block_path)?.flatten() {
            if UEVENT::parse(device.path().join("uevent"))
                .is_some_and(|uevent| is_correct_disk(&uevent))
            {
                detected_devices.push(device.file_name().to_string_lossy().into_owned())
            }
        }

        Ok(detected_devices)
    }
}

pub mod partition {
    use super::{io, Partition};

    pub fn fetch_disk_partitions(disk: &mut super::Disk) -> io::Result<()> {
        Partition::fetch_all(disk)
    }
}
