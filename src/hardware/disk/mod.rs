//! Fetch information about installed disks
//!
//! Feature `disk` must be enabled
//!
//! Supported platforms: Linux

#![cfg(feature = "disk")]
#![allow(unreachable_code)]

mod partition;
pub use partition::*;
mod filesystem;
pub use filesystem::*;

use crate::{types::Memory, util::error::unsupported_os};

use std::{
    io::{self, Error, ErrorKind},
    path::PathBuf,
};

/// Contains SSD and HDD \
/// (don't trust this thing because usb sticks are also recognized as HDD)
#[derive(Clone, Debug, Default, PartialEq)]
pub enum DiskTechnology {
    #[default]
    HDD,
    SSD,
}

/// GUID Partition Table signature (512-519 bytes)
pub const GPT_SIGNATURE: [u8; 8] = [69, 70, 73, 32, 80, 65, 82, 84];

/// Contains basic information about disk
#[derive(Clone, Debug, Default)]
pub struct Disk {
    /// Device path to disk \
    /// e.g. "/dev/nvme0n1" or "/dev/sda"
    pub dev_path: PathBuf,
    /// Disk model
    pub model: Option<String>,
    /// Disk size
    pub size: Memory,
    /// Partition on disk
    pub partitions: Vec<Partition>,
    /// SSD or HDD
    pub technology: DiskTechnology,
    /// Disk is removable
    pub removable: bool,
    /// Partition table type
    pub partition_table_type: Option<PartitionTableType>,
}

impl Disk {
    /// I don't know why this function is here, it's useless i think
    pub fn is_ssd(&self) -> bool {
        self.technology == DiskTechnology::SSD
    }
    /// Fetch partition table type \
    /// this function updates [`Disk`]'s field [`Disk::partition_table_type`]
    /// root access is required, otherwise you will get an error "Permission denied"
    pub fn fetch_table_type(&mut self) -> io::Result<()> {
        #[cfg(target_os = "linux")]
        return linux::disk::partition_table_type(self);

        unsupported_os!();
    }
    /// Fetch all partitions on disk
    pub fn fetch_partitions(&mut self) -> io::Result<()> {
        #[cfg(target_os = "linux")]
        return linux::partition::fetch_disk_partitions(self);

        unsupported_os!();
    }
    /// Function under construction ! \
    /// Fetch filesystems on partitions \
    /// Make sure that disk contains partitions (run [`Disk::fetch_partitions`] ) and table type (run [`Disk::fetch_table_type`]) \
    /// Currently only MBR disks supported
    pub fn fetch_filesystems(&mut self) -> io::Result<Option<Filesystem>> {
        #[cfg(target_os = "linux")]
        return linux::disk::identify_filesystems(self);

        unsupported_os!();
    }
    /// Sort [`Disk::partitions`] by number
    pub fn sort_partitions(&mut self) {
        self.partitions
            .sort_by(|partition_a, partition_b| partition_a.number.cmp(&partition_b.number));
    }
}

mod linux;

/// Returns [`Vec`]<[`Disk`]> \
/// A [`Vec`] with all disks found in system \
/// Uses a platform-specific function \
/// If platform isn't supported it will return error [`ErrorKind::Unsupported`]
pub fn fetch_all() -> io::Result<Vec<Disk>> {
    #[cfg(target_os = "linux")]
    return linux::disk::fetch_disks();

    unsupported_os!();
}

/// Returns [`Vec`]<[`String`]> \
/// A [`Vec`] with all disk device names \
/// Pass them to [`fetch_disk_info`] to get [`Disk`]
/// Uses a platform-specific function \
/// If platform isn't supported it will return error [`ErrorKind::Unsupported`]
pub fn fetch_devices() -> io::Result<Vec<String>> {
    #[cfg(target_os = "linux")]
    return linux::disk::fetch_devices();

    unsupported_os!();
}

/// Returns [`Disk`] \
/// Uses a platform-specific function \
/// If platform isn't supported it will return error [`ErrorKind::Unsupported`]
///
/// `device` argument must be block device path or name \
/// e.g. "/dev/sda" or "sda"
///
/// Make sure thet `device` has the correct format (see above) \
/// Otherwise, use `fetch_devices` and `fetch_all`
pub fn fetch_disk_info<T>(device: T) -> io::Result<Disk>
where
    T: ToString,
{
    #[cfg(target_os = "linux")]
    return linux::disk::fetch_disk_info(device);

    unsupported_os!();
}
