use crate::types::Memory;

use std::{io::Result, path::PathBuf};

/// Contains most popular partition table types
#[derive(Clone, Debug, PartialEq)]
pub enum PartitionTableType {
    /// GUID Partition Table
    GPT,
    /// Master Boot Record
    MBR,
}

/// Contains basic information about partition on disk
#[derive(Clone, Debug)]
pub struct Partition {
    /// Device path to partition \
    /// e.g. "/dev/nvme0n1p1" or "/dev/sda1"
    pub dev_path: PathBuf,
    /// Disk device path \
    /// e.g. "/dev/nvme0n1" or "/dev/sda"
    pub disk_dev_path: PathBuf,
    /// Partition number
    pub number: u8,
    /// Partition size
    pub size: Memory,
    /// Partition is read only
    pub readonly: bool,
}

impl Partition {
    pub fn identify_filesystem(&mut self) -> Result<()> {
        Ok(())
    }
}
