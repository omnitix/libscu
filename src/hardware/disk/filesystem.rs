#![allow(non_camel_case_types)]

#[derive(Clone, Debug)]
pub enum Filesystem {
    BTRFS,
    exFAT_NTFS_HPFS,
    ext2,
    ext3,
    ext4,
    FAT12,
    FAT16,
    FAT32,
    EFI,
}

impl Filesystem {
    pub fn from_mbr_byte(byte: u8) -> Option<Self> {
        for fs_id in crate::util::data::filesystems::FILESYSTEM_MBR_BYTES {
            if byte == fs_id.1 {
                return Some(fs_id.0);
            }
        }

        None
    }
}
