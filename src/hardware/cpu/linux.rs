#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::{
    types,
    util::{
        self,
        error::{invalid_data, not_found},
        fs::read_file,
        platform::linux::libc::get_nprocs_conf,
    },
};

use super::{CPUInfo, CPUStats, CPUVendor};

use std::{
    fs::{self, OpenOptions},
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn parse_proc_cpuinfo(cpuinfo: &mut CPUInfo) -> std::io::Result<()> {
    let file = OpenOptions::new().read(true).open("/proc/cpuinfo")?;
    let mut cpuinfo_read = BufReader::new(file);
    let mut line_buf = String::new();
    while cpuinfo_read.read_line(&mut line_buf).is_ok_and(|b| b > 0) {
        if line_buf.ends_with("\n\n") {
            break; // Exit after first block
        }

        let (var, val) = line_buf.split_once(':').unwrap_or(("", ""));
        let (var, val) = (var.trim(), val.trim().to_string());
        if var.is_empty() || val.is_empty() {
            continue;
        }
        match var {
            "model name" | "Hardware" => {
                cpuinfo.model = util::string::remove_multiple_spaces(&val);
            }
            "vendor_id" => cpuinfo.vendor = CPUVendor::from_string(&val),
            "cpu cores" => {
                if let Ok(cores) = val.trim().parse::<u8>() {
                    cpuinfo.cores = cores;
                }
            }
            "cpu MHz" => {
                let freq = util::string::extract_u64(val.split(".").next().unwrap_or("0"));
                cpuinfo.frequency = types::Frequency::from_mhz(freq as u32);
            }
            "cpu" => {
                if val.contains("POWER9") {
                    cpuinfo.model = "POWER9".to_string();
                    cpuinfo.vendor = CPUVendor::IBM;
                }
            }
            _ => {}
        }

        line_buf.clear();
    }
    Ok(())
}

pub fn fetch_max_freq(freq: &mut types::Frequency) {
    for file in [
        "base_frequency",
        "bios_limit",
        "scaling_max_freq",
        "cpuinfo_max_freq",
    ] {
        if let Ok(freqfile) =
            fs::read_to_string(format!("/sys/devices/system/cpu/cpu0/cpufreq/{file}"))
        {
            *freq = types::Frequency::from_hz(util::string::extract_u64(&freqfile));
            break;
        }
    }
}

pub fn fetch_threads_count(threads: &mut u8) {
    unsafe {
        let _threads = get_nprocs_conf() as u8;
        if _threads > *threads {
            *threads = _threads;
        }
    }
}

pub fn fetch_temperature(temp: &mut Option<f32>) {
    if let Ok(listdir) = util::fs::list_dir_from_str("/sys/class/hwmon", true) {
        for hwmon in listdir {
            let hwmon = PathBuf::from("/sys/class/hwmon").join(hwmon);
            if fs::metadata(hwmon.join("name")).is_err() {
                continue;
            }
            if ["k10temp", "coretemp"].contains(
                &fs::read_to_string(hwmon.join("name"))
                    .unwrap_or_default()
                    .trim(),
            ) {
                let mut hwmon_files_sorted =
                    util::fs::list_dir(hwmon.clone(), true).unwrap_or_default();
                hwmon_files_sorted.sort_by(|a, b| {
                    a.file_name()
                        .unwrap()
                        .to_ascii_lowercase()
                        .cmp(&b.file_name().unwrap().to_ascii_lowercase())
                });

                for f in hwmon_files_sorted {
                    if let Some(fname) = f.file_name().and_then(std::ffi::OsStr::to_str) {
                        if fname.starts_with("temp") && fname.ends_with("_input") {
                            if let Ok(fread) = fs::read_to_string(f) {
                                if let Ok(parsed) = fread.trim().parse::<u32>() {
                                    *temp = Some(parsed as f32 / 1000.0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

#[cfg(target_os = "linux")]
pub mod multicpu_support {
    use super::{super::Unit, types::Frequency, CPUInfo, CPUVendor};

    fn parse_unit_to_cpuinfo(unit_data: &str) -> Option<CPUInfo> {
        let mut cpuinfo = CPUInfo::default();

        for line in unit_data.split("\n") {
            let Some((key, value)) = line
                .split_once(":")
                .map(|(key, val)| (key.trim(), val.trim()))
            else {
                continue;
            };
            match key {
                "vendor_id" => cpuinfo.vendor = CPUVendor::from_string(value),
                "model name" => cpuinfo.model = value.to_string(),
                "siblings" => {
                    if let Ok(threads) = value.parse::<u8>() {
                        cpuinfo.threads = threads;
                    }
                }
                "cpu cores" => {
                    if let Ok(cores) = value.parse::<u8>() {
                        cpuinfo.cores = cores;
                    }
                }
                "cpu MHz" => {
                    if let Ok(freq_float) = value.parse::<f64>() {
                        cpuinfo.frequency = Frequency::from_mhz(freq_float as u32)
                    }
                }
                _ => {}
            }
        }

        Some(cpuinfo)
    }

    pub fn parse_multicpu(cpuinfo: &str) -> Vec<Unit> {
        use std::collections::HashMap;
        let mut cpus: Vec<Unit> = Vec::default();
        let mut units: HashMap<u64, CPUInfo> = HashMap::new();

        fn id_lookup(unit_data: &str) -> Option<u64> {
            for line in unit_data.split("\n") {
                if line.starts_with("physical id") {
                    if let Some(id) = line.split_once(":") {
                        if let Ok(id_integer) = id.1.replace(" ", "").parse::<u64>() {
                            return Some(id_integer);
                        }
                    }
                }
            }
            None
        }

        for unit in cpuinfo.split("\n\n") {
            let Some(id) = id_lookup(unit) else {
                continue;
            };

            if let std::collections::hash_map::Entry::Vacant(e) = units.entry(id) {
                if let Some(cpuinfo) = parse_unit_to_cpuinfo(unit) {
                    e.insert(cpuinfo);
                }
            }
        }

        for (id, cpuinfo) in units.into_iter() {
            cpus.push(Unit {
                physical_id: id,
                cpuinfo,
            });
        }
        cpus.sort_by(|unit_a, unit_b| unit_a.physical_id.cmp(&unit_b.physical_id));

        cpus
    }
    #[cfg(test)]
    mod multicpu_tests {
        use super::parse_multicpu;

        fn multicpu(proc_cpuinfo: &String) {
            use std::time::{SystemTime, UNIX_EPOCH};

            let start = SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .expect("Time went backwards")
                .as_micros();
            let cpus = parse_multicpu(proc_cpuinfo);
            let end = SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .expect("Time went backwards")
                .as_micros();

            println!("{cpus:#?}");

            println!("parsing took {} microsecs", end - start);
        }

        #[test]
        fn multicpu_from_testfile() {
            let cpuinfo = std::fs::read_to_string("./multicpu_cpuinfo").unwrap();

            multicpu(&cpuinfo)
        }

        #[test]
        fn multicpu_from_system() {
            let cpuinfo = std::fs::read_to_string("/proc/cpuinfo").unwrap();

            multicpu(&cpuinfo)
        }
    }
}

impl CPUStats {
    fn idle(&self) -> u64 {
        self.idle.saturating_add(self.iowait)
    }
    fn nonidle(&self) -> u64 {
        self.user
            .saturating_add(self.nice)
            .saturating_add(self.system)
            .saturating_add(self.irq)
            .saturating_add(self.softirq)
            .saturating_add(self.steal)
    }

    fn find_cpu_line(stat_read: &mut BufReader<std::fs::File>) -> Option<String> {
        let mut line_buf = String::new();
        while stat_read.read_line(&mut line_buf).is_ok_and(|b| b != 0) {
            if line_buf.starts_with("cpu ") {
                return Some(line_buf.trim_end().to_string());
            }
        }
        None
    }
    pub fn read() -> std::io::Result<Self> {
        let mut stats = Self::default();

        let file = read_file("/proc/stat")?;
        let mut stat_read = BufReader::new(file);
        let cpu_stats_line = Self::find_cpu_line(&mut stat_read)
            .ok_or(not_found!("line starting with `cpu ` not found."))?;
        let mut cpu_stats = cpu_stats_line.split_whitespace();
        let _ = cpu_stats.next();

        for (stat_buf, stat_name) in [
            (&mut stats.user, "user"),
            (&mut stats.nice, "nice"),
            (&mut stats.system, "system"),
            (&mut stats.idle, "idle"),
            (&mut stats.iowait, "iowait"),
            (&mut stats.irq, "irq"),
            (&mut stats.softirq, "softirq"),
            (&mut stats.steal, "steal"),
            (&mut stats.guest, "guest"),
            (&mut stats.guest_nice, "guest_nice"),
        ] {
            match cpu_stats.next().map(|int| int.parse::<u64>()) {
                Some(Ok(stat)) => *stat_buf = stat as u64,
                _ => {
                    return Err(invalid_data!(format!(
                    "statistics for `{stat_name}` value not found or cannot be parsed as integer."
                )))
                }
            }
        }

        Ok(stats)
    }
    pub fn percentage_usage(&self, prev: &Self) -> f32 {
        let prev_idle = prev.idle();
        let idle = self.idle();

        let prev_total = prev_idle.saturating_add(prev.nonidle());
        let total = idle.saturating_add(self.nonidle());

        let totald = total.saturating_sub(prev_total);
        let idled = idle.saturating_sub(prev_idle);

        let percentage = totald.saturating_sub(idled) as f64 / totald as f64;
        (percentage * 100.).min(100.) as f32
    }
}
