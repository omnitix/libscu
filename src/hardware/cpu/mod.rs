//! Fetch information about installed CPU
//!
//! Feature `cpu` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(feature = "cpu")]

#[allow(unused_imports)]
use crate::{
    types,
    util::{
        self,
        error::{not_found, unsupported_os},
        log_err,
    },
};

mod android;
mod linux;
mod macos;

use regex_lite::Regex;

/// Contains basic information about CPU
#[derive(Clone, Debug, Default, PartialEq)]
pub struct CPUInfo {
    pub vendor: CPUVendor,
    pub model: String,
    pub frequency: types::Frequency,
    pub cores: u8,
    pub threads: u8,
    pub temperature: Option<f32>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum CPUVendor {
    AMD,
    Apple,
    HuaweiKirin,
    IBM,
    Intel,
    Mediatek,
    Qualcomm,
    SamsungExynos,
    Unisoc,
    Another(String),
}

impl Default for CPUVendor {
    fn default() -> Self {
        Self::Another("unknown".to_string())
    }
}

impl std::fmt::Display for CPUVendor {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            fmt,
            "{}",
            match self {
                Self::AMD => "AMD".to_string(),
                Self::Apple => "Apple".to_string(),
                Self::HuaweiKirin => "Huawei Kirin".to_string(),
                Self::IBM => "IBM".to_string(),
                Self::Intel => "Intel".to_string(),
                Self::Mediatek => "Mediatek".to_string(),
                Self::Qualcomm => "Qualcomm".to_string(),
                Self::SamsungExynos => "Samsung Exynos".to_string(),
                Self::Unisoc => "Unisoc".to_string(),
                Self::Another(another) => another.clone(),
            }
        )
    }
}

impl CPUVendor {
    pub fn from_string(str: &str) -> Self {
        match str {
            "AuthenticAMD" | "AMD" => Self::AMD,
            "Apple" => Self::Apple,
            "HUAWEI" => Self::HuaweiKirin,
            "IBM" => Self::IBM,
            "GenuineIntel" | "Intel" => Self::Intel,
            "Mediatek" | "MTK" => Self::Mediatek,
            "Qualcomm" | "QTI" => Self::Qualcomm,
            "Samsung" | "samsung" => Self::SamsungExynos,
            "Unisoc" => Self::Unisoc,
            another => Self::Another(another.to_string()),
        }
    }
}

fn cut_model_name_trash(cpuinfo: &mut CPUInfo) {
    for re_trash in [
        "(Dual|Quad|Six|Eight)-Core",
        "[[:digit:]]? COMPUTE CORES",
        "[[:digit:]]\\w\\+[[:digit:]]\\w",
        "[[:digit:]]+-Core", // "2-Core", "4-Core", etc.
        "\\((R|r)\\)",
        "\\((TM|tm)\\)",
        "R[[:digit:]]",
    ] {
        let re = Regex::new(re_trash).unwrap();
        if let Some(m) = re.find(&cpuinfo.model) {
            cpuinfo.model = cpuinfo.model.replace(m.as_str(), "")
        }
    }
    for trash in [
        ",",
        ".",
        "AMD",
        "based",
        "CPU",
        "Graphics",
        "Intel",
        "Mobile",
        "Processor",
        "Radeon",
        "RADEON",
        "redwood",
        "Series",
        "Technologies, Inc",
        "Vega",
        "with",
    ] {
        cpuinfo.model = cpuinfo.model.replace(trash, "")
    }
}
fn extract_model_name(cpuinfo: &mut CPUInfo) {
    cpuinfo.model = util::string::remove_multiple_spaces(
        cpuinfo
            .model
            .split('@')
            .next()
            .unwrap_or(&cpuinfo.model)
            .trim(),
    );
}
fn extract_vendor_from_model(cpuinfo: &mut CPUInfo) {
    for vendor in ["Apple", "AMD", "Intel", "Qualcomm", "Unisoc"] {
        if cpuinfo.model.contains(vendor) {
            cpuinfo.model = cpuinfo.model.replace(vendor, "");
            cpuinfo.vendor = CPUVendor::from_string(vendor);
            break;
        }
    }
}
fn beautify(cpuinfo: &mut CPUInfo) {
    cpuinfo.model = cpuinfo.model.trim().to_string();
    cpuinfo.model = cpuinfo
        .model
        .split_whitespace()
        .fold(String::new(), |a, b| a + b + " ")
        .trim_end()
        .into();
}

/// Used for multicpu systems \
/// Contains physical identifier of the CPU and information about CPU
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Unit {
    pub physical_id: u64,
    pub cpuinfo: CPUInfo,
}

/// Returns [`CPUInfo`]
///
/// * raw_model arg - return [`CPUInfo::model`] without any processing (as is)
pub fn fetch_info(raw_model: bool) -> std::io::Result<CPUInfo> {
    let mut result = CPUInfo::default();

    #[cfg(any(target_os = "linux", target_os = "android"))]
    {
        linux::parse_proc_cpuinfo(&mut result)?;
        #[cfg(target_os = "android")]
        {
            android::fetch_cpu_model(&mut result);
            android::fetch_cpu_vendor(&mut result);
        }
        linux::fetch_threads_count(&mut result.threads);
        linux::fetch_max_freq(&mut result.frequency);
        linux::fetch_temperature(&mut result.temperature);
    }
    #[cfg(target_os = "macos")]
    {
        let (_, _, _, _) = (
            log_err("CPU/Model", macos::fetch_cpu_model(&mut result.model)),
            log_err("CPU/Cores", macos::fetch_cores_count(&mut result.cores)),
            log_err("CPU/Threads", macos::fetch_threads(&mut result.threads)),
            log_err("CPU/Frequency", macos::fetch_freq(&mut result.frequency)),
        );
    }

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();

    extract_vendor_from_model(&mut result);
    // beautify CPUInfo
    if !raw_model {
        extract_model_name(&mut result);
        cut_model_name_trash(&mut result);
    }
    beautify(&mut result);

    // if failed to fetch cores
    if result.cores == 0 && result.threads != 0 {
        result.cores = result.threads;
    }

    Ok(result)
}

/// This function may be very unstable because it is in test mode
///
/// Returns [`Vec`]<[`Unit`]>
pub fn fetch_multicpus(raw_model: bool) -> std::io::Result<Vec<Unit>> {
    #[cfg(target_os = "linux")]
    {
        let proc_cpuinfo = std::fs::read_to_string("/proc/cpuinfo")?;
        let mut cpus: Vec<Unit> = linux::multicpu_support::parse_multicpu(&proc_cpuinfo);

        for cpu in cpus.iter_mut() {
            // beautify CPUInfo
            if !raw_model {
                extract_model_name(&mut cpu.cpuinfo);
                cut_model_name_trash(&mut cpu.cpuinfo);
            }
            extract_vendor_from_model(&mut cpu.cpuinfo);
            beautify(&mut cpu.cpuinfo);
        }

        if cpus.is_empty() {
            Err(not_found!(
                "not found any CPU in /proc/cpuinfo (maybe [FIXME] ?)"
            ))
        } else {
            Ok(cpus)
        }
    }

    #[cfg(not(target_os = "linux"))]
    crate::util::error::unsupported_os!();
}

#[derive(Clone, Copy, Default, Debug)]
pub(crate) struct CPUStats {
    pub user: u64,
    pub nice: u64,
    pub system: u64,
    pub idle: u64,
    pub iowait: u64,
    pub irq: u64,
    pub softirq: u64,
    pub steal: u64,
    pub guest: u64,
    pub guest_nice: u64,
}
/// Get CPU usage
/// * Linux-only
#[derive(Clone, Copy)]
pub struct CPUUsage {
    stats: CPUStats,
    prev_stats: CPUStats,
}
impl CPUUsage {
    /// Attempts to read /proc/stat on linux for the first time \
    /// You will need to call [`CPUUsage::update()`] at least one more time to get the correct usage from [`CPUUsage::usage()`]
    pub fn new() -> std::io::Result<Self> {
        #[cfg(target_os = "linux")]
        return Ok(Self {
            stats: CPUStats::read()?,
            prev_stats: CPUStats::default(),
        });

        #[cfg(not(target_os = "linux"))]
        unsupported_os!()
    }
    /// Update internal statistics to calculate usage in future
    pub fn update(&mut self) -> std::io::Result<()> {
        let new_stats = CPUStats::read()?;

        self.prev_stats = self.stats.clone();
        self.stats = new_stats;

        Ok(())
    }
    /// Returns CPU usage in percents
    pub fn usage(&self) -> std::io::Result<f32> {
        Ok(self.stats.percentage_usage(&self.prev_stats))
    }
}
