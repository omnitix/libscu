#![cfg(target_os = "macos")]
#![allow(unused_imports)]

use crate::types::Frequency;
use crate::util::platform::macos::libc;

use sysctl::{Ctl, Sysctl};

use std::ffi::CStr;
use std::io::{Error, ErrorKind, Result};
use std::ops::Deref;

pub fn fetch_cpu_model(model_buf: &mut String) -> Result<()> {
    let model = Ctl::new("machdep.cpu.brand_string")
        .unwrap()
        .value_string()
        .unwrap();

    *model_buf = model;
    Ok(())
}
pub fn fetch_cores_count(cores_buf: &mut u8) -> Result<()> {
    *cores_buf = Ctl::new("hw.physicalcpu_max")
        .unwrap()
        .value_string()
        .unwrap()
        .parse::<u8>()
        .unwrap();

    Ok(())
}
pub fn fetch_threads(threads: &mut u8) -> Result<()> {
    *threads = Ctl::new("hw.ncpu")
        .unwrap()
        .value_string()
        .unwrap()
        .parse::<u8>()
        .unwrap();

    Ok(())
}

pub fn fetch_freq(freq: &mut Frequency) -> Result<()> {
    #[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
    unsafe {
        let mut mhz: u32 = 0;
        let status = libc::silicon_freq(&mut mhz);
        if status.is_null() {
            *freq = Frequency::from_mhz(mhz);
            return Ok(());
        } else {
            let last_os_error = Error::last_os_error().to_string();
            return Err(Error::new(
                ErrorKind::Other,
                CStr::from_ptr(status)
                    .to_str()
                    .map(|e| e.to_string())
                    .unwrap_or(last_os_error),
            ));
        }
    }

    #[cfg(target_arch = "x86_64")]
    {
        *freq = Frequency::from_hz(
            Ctl::new("hw.cpufrequency")
                .unwrap()
                .value_string()
                .unwrap()
                .parse::<u64>()
                .unwrap()
                / 1000,
        );
        return Ok(());
    }
}
