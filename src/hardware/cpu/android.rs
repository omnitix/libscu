#![cfg(target_os = "android")]

use crate::util::platform::android::getprop;

use super::{CPUInfo, CPUVendor};

fn match_vendor(vendor: &str) -> Option<&'static str> {
    match vendor {
        "MTK" => Some("Mediatek"),
        "QTI" | "QUALCOMM" => Some("Qualcomm"),
        "Samsung" | "samsung" => Some("Exynos"),
        "HUAWEI" => Some("Kirin"),
        _ => None,
    }
}

pub fn fetch_cpu_vendor(cpuinfo: &mut CPUInfo) {
    let mut raw_vendor = String::new();
    for vendor_prop in [
        "Build.BRAND",
        "ro.soc.manufacturer",
        "ro.product.product.manufacturer",
    ] {
        if let Ok(vendor) = getprop(vendor_prop) {
            if vendor.is_empty() {
                continue;
            }

            if let Some(vendor) = match_vendor(&vendor) {
                cpuinfo.vendor = CPUVendor::from_string(vendor);
                break;
            };
        }
    }
}

fn extract_vendor_from_model(cpuinfo: &mut CPUInfo) {
    if cpuinfo.model.starts_with("Kirin") {
        cpuinfo.vendor = CPUVendor::HuaweiKirin;
        cpuinfo.model = cpuinfo.model[6..].to_string();
    }
}

pub fn fetch_cpu_model(cpuinfo: &mut CPUInfo) {
    if !cpuinfo.model.is_empty() {
        return;
    }

    if let Ok(model) = getprop("ro.soc.model") {
        // Exynos/Qualcomm
        cpuinfo.model = model.trim().to_string();
    }

    if cpuinfo.model.is_empty() {
        if let Ok(display_name) = getprop("ro.config.cpu_info_display") {
            // Huawei kirin
            cpuinfo.model = display_name.replace("HUAWEI ", "");
            extract_vendor_from_model(cpuinfo);
        } else if let Ok(model) = getprop("ro.vendor.mediatek.platform") {
            // Mediatek
            cpuinfo.model = model.trim().to_string();
        }
    }
}
