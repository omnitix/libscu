use std::fmt;

#[derive(Clone, Debug, Default, PartialEq)]
pub enum BatteryTechnology {
    #[default]
    Unknown,
    NiMH,
    LiIon,
    LiPoly,
    LiFe,
    NiCd,
    LiMn,
}

impl BatteryTechnology {
    pub fn from_str(str: &str) -> Self {
        // from sysfs (see linux sources drivers/power/supply/power_supply_sysfs.c)
        match str {
            "NiMH" => Self::NiMH,
            "Li-ion" => Self::LiIon,
            "Li-poly" => Self::LiPoly,
            "LiFe" => Self::LiFe,
            "NiCd" => Self::NiCd,
            "LiMn" => Self::LiMn,
            _ => Self::Unknown,
        }
    }
}

impl fmt::Display for BatteryTechnology {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}",
            match self {
                Self::Unknown => "Unknown",
                Self::NiMH => "Nickel-metal hydride",
                Self::LiIon => "Lithium ion",
                Self::LiPoly => "Lithium polymer",
                Self::LiFe => "LiFePO4",
                Self::NiCd => "Nickel-cadmium",
                Self::LiMn => "Li-ion manganese",
            }
        )
    }
}
