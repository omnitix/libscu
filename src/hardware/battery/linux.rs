#![cfg(target_os = "linux")]

use super::{BatteryInfo, BatteryStatus, BatteryTechnology};

use crate::util::fs::{get_prop, list_dir_from_str};

use std::{
    io::{Error, ErrorKind, Result},
    path::Path,
};

const SYSFS_POWER_SUPPLY: &str = "/sys/class/power_supply";

pub fn collect_info(battery_interface: &str) -> Result<BatteryInfo> {
    let battery_interface_path = Path::new(SYSFS_POWER_SUPPLY).join(battery_interface);
    if Path::new(SYSFS_POWER_SUPPLY)
        .join(battery_interface)
        .exists()
    {
        let result = BatteryInfo {
            manufacturer: get_prop(&battery_interface_path, "manufacturer").ok(),
            model: get_prop(&battery_interface_path, "model_name")
                .unwrap_or(battery_interface.to_string()),
            technology: BatteryTechnology::from_str(
                &get_prop(&battery_interface_path, "technology").unwrap_or_default(),
            ),
            level: get_prop(&battery_interface_path, "capacity")
                .and_then(|level| {
                    level.parse::<u64>().map_err(|_| {
                        Error::new(
                            ErrorKind::InvalidData,
                            format!("invalid battery level data: {level}"),
                        )
                    })
                })?
                .clamp(0, 100) as u8,
            status: BatteryStatus::from_prop(
                &get_prop(&battery_interface_path, "status").unwrap_or_default(),
            ),
        };

        return Ok(result);
    }

    Err(Error::from(ErrorKind::NotFound))
}

/// Returns [`Vec`]<[`String`]> with battery interfaces \
/// You will pass battery interface to [`collect_info`] to get [`BatteryInfo`]
pub fn fetch_interfaces() -> Result<Vec<String>> {
    let mut result = Vec::<String>::new();

    if let Ok(sysfs_power_supply) = list_dir_from_str("/sys/class/power_supply", true) {
        sysfs_power_supply
            .iter()
            .map(|interface| {
                interface
                    .file_name()
                    .unwrap_or_default()
                    .to_str()
                    .unwrap_or_default()
            })
            .filter(|interface_name| interface_name.starts_with("BAT"))
            .for_each(|bat| result.push(bat.to_string()));
    }

    if result.is_empty() {
        Err(Error::from(ErrorKind::NotFound))
    } else {
        Ok(result)
    }
}
