//! Fetch information about installed batteries
//!
//! Feature `battery` must be enabled
//!
//! Supported platforms: Linux

#![cfg(feature = "battery")]

mod status;
pub use status::BatteryStatus;
mod technology;
pub use technology::BatteryTechnology;

/// Contains basic information about battery
#[derive(Clone, Debug, Default)]
pub struct BatteryInfo {
    pub manufacturer: Option<String>,
    pub model: String,
    pub technology: BatteryTechnology,
    pub level: u8,
    pub status: BatteryStatus,
}

mod linux;

use std::io;

pub fn collect_info(battery_interface: &str) -> io::Result<BatteryInfo> {
    #[cfg(target_os = "linux")]
    return linux::collect_info(battery_interface);

    #[cfg(not(target_os = "linux"))]
    crate::util::error::unsupported_os!();
}

/// Returns [`Vec`]<[`String`]> with battery interfaces \
/// You will pass battery interface to [`collect_info`] to get [`BatteryInfo`]
pub fn fetch_interfaces() -> io::Result<Vec<String>> {
    #[cfg(target_os = "linux")]
    return linux::fetch_interfaces();

    #[cfg(not(target_os = "linux"))]
    crate::util::error::unsupported_os!();
}
