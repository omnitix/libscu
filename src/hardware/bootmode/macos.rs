#![cfg(target_os = "macos")]

use super::BootMode;

/// Returns [`BootMode`]
pub fn fetch_mode() -> BootMode {
    BootMode::UEFI
}
