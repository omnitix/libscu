#![cfg(target_os = "linux")]

#[cfg(feature = "pci_ids")]
use crate::util::data::pci_ids;
use crate::util::fs;
#[cfg(feature = "pci_ids_parser")]
use crate::util::pci_ids_parser;

use super::{GPUInfo, GPUVendor};

use regex_lite::Regex;

fn get_val(str: &str, splitter: &str) -> String {
    str.split(splitter)
        .nth(1)
        .unwrap_or_default()
        .trim()
        .to_string()
}

fn parse_uevent(gpuinfo: &mut GPUInfo, uevent: String) {
    for line in uevent.split('\n') {
        match line.split("=").next().unwrap_or_default() {
            "DRIVER" => {
                let val = get_val(line, "DRIVER=");
                gpuinfo.driver = if !val.is_empty() { Some(val) } else { None }
            }
            "PCI_ID" => {
                let pci_id = get_val(line, "PCI_ID=");
                gpuinfo.vendor = GPUVendor::from_str(&get_vendor(pci_id.as_str()));
                gpuinfo.model = pci_id.to_ascii_lowercase();
            }
            "PCI_SUBSYS_ID" => {
                let val = get_val(line, "PCI_SUBSYS_ID=");
                if !val.is_empty() {
                    gpuinfo.model.push_str(format!(" {val}").as_str())
                }
            }
            _ => {}
        }
    }
}

fn get_vendor(pci_id: &str) -> String {
    match pci_id
        .split(':')
        .next()
        .unwrap_or_default()
        .to_ascii_lowercase()
        .as_str()
    {
        "10de" => "NVIDIA",
        "1002" => "AMD",
        "8086" => "Intel",
        "1a03" => "ASPEED",
        "15ab" => "VMWare",
        "1af4" => "RedHat",
        other => other,
    }
    .to_string()
}

fn remove_trash(gpuinfo: &mut GPUInfo) {
    for trash in [
        "Atom",
        "Processor",
        "Graphics & Display",
        "Series",
        "Chipset",
        "Integrated",
        "Controller",
    ] {
        gpuinfo.model = gpuinfo.model.replace(trash, "")
    }
}

pub fn fetch_all(result_buf: &mut Vec<GPUInfo>, raw_model: bool) -> std::io::Result<()> {
    let pci_devices = fs::list_dir_from_str("/sys/bus/pci/devices", true)?;
    for entry in pci_devices {
        let mut gpuinfo = GPUInfo::default();
        if !fs::get_prop(&entry, "class")
            .unwrap_or_default()
            .starts_with("0x03")
        {
            continue;
        }
        let uevent = fs::get_prop(&entry, "uevent").unwrap_or("".to_string());
        let mut hwmon_path = entry.join("hwmon");
        if let Ok(hwmon_entries) = fs::list_dir(hwmon_path.clone(), true) {
            for hwentry in hwmon_entries {
                let hwentry = hwentry.file_name().unwrap_or_default();
                if Regex::new("hwmon[[:digit:]]")
                    .unwrap()
                    .is_match(hwentry.to_str().unwrap_or_default())
                {
                    hwmon_path = entry.join("hwmon").join(hwentry);
                }
            }
        }
        if !uevent.is_empty() {
            if let Ok(parsed_temp) =
                fs::get_prop(&hwmon_path, "temp1_input").map(|temp| temp.parse::<u32>())
            {
                gpuinfo.temperature = parsed_temp.map(|temp| temp as f32 / 1000.0).ok();
            }

            parse_uevent(&mut gpuinfo, uevent);

            if !gpuinfo.model.is_empty() {
                #[cfg(all(feature = "pci_ids_parser", not(feature = "pci_ids")))]
                {
                    if let Ok(found_model) = pci_ids_parser::lookup_for_pci_id_value(&gpuinfo.model)
                    {
                        gpuinfo.model = found_model;
                    }
                }
                #[cfg(all(feature = "pci_ids", not(feature = "pci_ids_parser")))]
                {
                    gpuinfo.model = gpuinfo.model.to_ascii_lowercase();
                    if pci_ids::contains(&gpuinfo.model) {
                        if let Some(name) = pci_ids::get(&gpuinfo.model.clone()) {
                            gpuinfo.model = name.to_string();
                        };
                    } else if gpuinfo.model.contains(' ')
                        && pci_ids::contains(&gpuinfo.model.split(' ').next().unwrap_or_default())
                    {
                        if let Some(name) = pci_ids::get(
                            &gpuinfo.model.clone().split(' ').next().unwrap_or_default(),
                        ) {
                            gpuinfo.model = name.to_string();
                        };
                    }
                }

                if !raw_model {
                    remove_trash(&mut gpuinfo);
                }
                super::beautify_model(&mut gpuinfo, raw_model);

                result_buf.push(gpuinfo);
            }
        }
    }
    Ok(())
}
