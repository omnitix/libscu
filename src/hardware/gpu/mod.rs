//! Fetch information about installed GPU
//!
//! Feature `gpu` must be enabled \
//! Feature `pci_ids` is also required for fetching correct GPU name, if not enabled you will get only PCI ID \
//!
//! Supported platforms: Linux

#![cfg(feature = "gpu")]

mod linux;

use crate::util;

// Contains most popular physical and virtual GPU vendors
#[derive(Clone, Debug, PartialEq)]
pub enum GPUVendor {
    Nvidia,
    AMD,
    Intel,
    ASPEED,
    VMWare,
    RedHat,
    UnknownId(String),
}

/// Contains basic information about GPU
#[derive(Clone, Debug, Default)]
pub struct GPUInfo {
    pub vendor: GPUVendor,
    pub model: String,
    pub driver: Option<String>,
    pub temperature: Option<f32>,
}

impl GPUVendor {
    pub fn from_str(str: &str) -> Self {
        match str {
            "NVIDIA" => Self::Nvidia,
            "AMD" => Self::AMD,
            "Intel" => Self::Intel,
            "ASPEED" => Self::ASPEED,
            "VMWare" => Self::VMWare,
            "RedHat" => Self::RedHat,
            another => Self::UnknownId(another.to_string()),
        }
    }
}

impl Default for GPUVendor {
    fn default() -> Self {
        Self::UnknownId("".to_string())
    }
}

impl std::fmt::Display for GPUVendor {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            fmt,
            "{}",
            match self {
                Self::UnknownId(pci_id) => format!("Unknown [{pci_id}]"),
                known => format!("{known:?}"),
            }
        )
    }
}

fn beautify_model(gpuinfo: &mut GPUInfo, raw_model: bool) {
    if !raw_model && gpuinfo.model.contains("[") && gpuinfo.model.contains("]") {
        gpuinfo.model = gpuinfo
            .model
            .split(']')
            .next()
            .unwrap_or(&gpuinfo.model)
            .split('[')
            .nth(1)
            .unwrap_or(&gpuinfo.model)
            .to_string();
    }

    gpuinfo.model = gpuinfo.model.replace(&gpuinfo.vendor.to_string(), "");

    gpuinfo.model = util::string::remove_multiple_spaces(&gpuinfo.model);
}

/// Returns vector with [`GPUInfo`]
///
/// raw_model arg - return [`GPUInfo::model`] without cutting trash and other processing (as is)
///
/// required features: pci_ids_parser or pci_ids \
/// model will be raw identifier if not found \
/// if `pci_ids_parser` enabled it will look for identifier in local pci.ids \
/// if `pci_ids` enabled libscu will ship huge array of preparsed pci.ids \
///
/// `pci_ids_parser` method is prioritized
pub fn fetch_all(raw_model: bool) -> std::io::Result<Vec<GPUInfo>> {
    let mut result = Vec::<GPUInfo>::new();

    #[cfg(target_os = "linux")]
    {
        linux::fetch_all(&mut result, raw_model)?;
        Ok(result)
    }

    #[cfg(not(target_os = "linux"))]
    crate::util::error::unsupported_os!();
}
