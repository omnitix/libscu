#![cfg(target_os = "macos")]

use crate::{types::Memory, util::platform::macos::libc};

use super::{RAMInfo, SWAPInfo};

use std::io;

use sysctl::{Ctl, Sysctl};

fn fetch_total() -> i64 {
    let bytes = Ctl::new("hw.memsize").unwrap().value_string().unwrap();

    bytes.parse::<u64>().unwrap() as i64
}

fn fetch_used() -> i64 {
    unsafe { libc::get_used_memory() as i64 }
}

fn fetch_swap_info() -> Option<(i64, i64)> {
    let mut swap: (u64, u64) = (0, 0);
    unsafe {
        if libc::get_swap_info(&mut swap.0, &mut swap.1) == 0 && swap.0 > 0 {
            return Some((swap.0 as i64, swap.1 as i64));
        }
    }

    None
}

pub fn fetch_info() -> io::Result<RAMInfo> {
    let total = fetch_total();
    let used = fetch_used();

    let swap = fetch_swap_info();

    Ok(RAMInfo {
        total: Memory::from_bytes(total),
        used: Memory::from_bytes(used),

        swap: swap.map(|swap_info| SWAPInfo::from_bytes(swap_info.0, swap_info.1)),
    })
}
