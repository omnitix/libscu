//! Fetch information about installed RAM
//!
//! Feature `ram` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(feature = "ram")]

use crate::types::Memory;

mod linux;
mod macos;

/// Contains information about RAM and Swap
#[derive(Clone, Debug, Default)]
pub struct RAMInfo {
    pub total: Memory,
    pub used: Memory,

    pub swap: Option<SWAPInfo>,
}

/// Contains information about SWAP
#[derive(Clone, Debug, Default)]
pub struct SWAPInfo {
    pub total: Memory,
    pub used: Memory,
}

impl SWAPInfo {
    pub fn from_bytes(total: i64, used: i64) -> Self {
        Self {
            total: Memory::from_bytes(total),
            used: Memory::from_bytes(used),
        }
    }
}

/// Returns [`RAMInfo`]
pub fn fetch_info() -> std::io::Result<RAMInfo> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::fetch_info();
    #[cfg(target_os = "macos")]
    return macos::fetch_info();

    #[cfg(not(any(target_os = "linux", target_os = "android", target_os = "macos")))]
    crate::util::error::unsupported_os!();
}
