// Required features: battery

use libscu::hardware::battery;

fn main() -> std::io::Result<()> {
    let batteries = battery::fetch_interfaces()?;
    if batteries.is_empty() {
        println!("No batteries detected");
    } else {
        for bat_interface in batteries {
            if let Ok(bat) = battery::collect_info(&bat_interface) {
                if let Some(manufacturer) = bat.manufacturer {
                    println!("Manufacturer: {manufacturer}");
                };
                println!("Model: {}", bat.model);
                println!("Level: {}%", bat.level);
                println!("Status: {}", bat.status.to_str());
                println!("Technology: {}", bat.technology);
            }
        }
    }
    Ok(())
}
