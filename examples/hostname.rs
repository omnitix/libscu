// Required features: hostname

use libscu::software::hostname;

fn main() {
    #[cfg(target_os = "macos")]
    let raw = true;

    if let Ok(hostname) = hostname::fetch(
        #[cfg(target_os = "macos")]
        raw,
    ) {
        println!("Hostname: {hostname}");
    }
}
