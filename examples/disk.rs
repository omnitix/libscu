// Required features: disk

use libscu::hardware::disk;

fn main() {
    if let Ok(all_drives) = disk::fetch_all() {
        for mut disk in all_drives {
            println!("Device: {:?}", disk.dev_path);
            if let Some(model) = disk.model.clone() {
                println!("- Model: {model}");
            }
            println!("- Size: {:.1}GiB", disk.size.gb);
            println!("- Technology: {:?}", disk.technology);
            println!("- Is removable: {}", disk.removable);
            if disk.fetch_table_type().is_ok() {
                println!("- Partition table: {:?}", disk.partition_table_type);
            }
            match disk.fetch_partitions() {
                Ok(_) => {
                    if !disk.partitions.is_empty() {
                        disk.sort_partitions();
                        println!("- Partitions:");
                        for partition in disk.partitions.clone().into_iter() {
                            println!("  Device: {}", partition.dev_path.to_string_lossy());
                            println!("  - Partition number: {}", partition.number);
                            println!("  - Size: {:.1}GiB", partition.size.gb);
                            println!("  - Is read-only: {}", partition.readonly);
                        }
                    }
                }
                Err(err) => eprintln!("encountered error during partitions fetching: {err:#?}"),
            }
        }
    } else {
        println!("Platform not supported.");
    }
}
