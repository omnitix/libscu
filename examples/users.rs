// Required features: users

use libscu::software::users;

fn print_user_info(user: users::User) {
    println!("User: {}", user.name);
    println!("- UID / GID: {} / {}", user.uid, user.gid);
    if let Some(home) = user.home_dir {
        println!("- Home directory: {home}")
    } else {
        println!("- Home directory not presented.")
    }
    if let Some(shell) = user.shell {
        println!("- Shell: {shell}")
    } else {
        println!("- Shell not presented");
    }
}

fn main() {
    #[cfg(target_os = "linux")]
    if let Ok(users) = users::fetch_all(true) {
        println!("Users with shells:");
        for user in users {
            print_user_info(user);
        }
        println!();
    }
    if let Ok(current_user) = users::fetch_current() {
        println!("Current user:");
        print_user_info(current_user);
    }
}
