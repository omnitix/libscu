// Required features: shell

use libscu::software::shell;

fn main() -> std::io::Result<()> {
    let fetch_version = true;
    let shell_info = shell::fetch_info(fetch_version)?;

    println!("Shell: {}", shell_info.name);
    if let Some(shell_version) = shell_info.version {
        println!("- Version: {}", shell_version);
    }

    Ok(())
}
