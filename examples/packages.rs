// Required features: packages

use libscu::software::packages;

fn main() {
    match packages::fetch_all_managers() {
        Ok(managers) => {
            if !managers.is_empty() {
                for package_manager in managers {
                    println!("Package manager: {}", package_manager.name);
                    println!(
                        "- Number of packages: {}",
                        package_manager.number_of_packages
                    );
                }
            } else {
                println!("No package managers found.");
            }
        }
        Err(error) => eprintln!("Failed to fetch package managers: {error:?}"),
    }
}
