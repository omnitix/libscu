// Required features: locale

use libscu::software::locale;

fn main() -> std::io::Result<()> {
    println!("LC_ALL: {}", locale::fetch()?);

    Ok(())
}
