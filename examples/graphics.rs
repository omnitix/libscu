// Required features: graphics

use libscu::software::graphics;

fn main() {
    #[cfg(target_os = "linux")]
    if let Ok(display_server) = graphics::fetch_display_server() {
        println!("Display server: {:?}", display_server);
    }
    if let Ok(desktop_environment) = graphics::fetch_environment() {
        println!("Desktop environment: {}", desktop_environment.to_str());
    }

    let fetch_version = true;
    if let Ok(window_manager) = graphics::fetch_window_manager(fetch_version) {
        if let Some(wm) = window_manager.name {
            println!("Window manager: {}", wm.to_string());
        }
        if let Some(version) = window_manager.version {
            println!("- Version: {}", version);
        }
    }
}
