use libscu::software::mounts;

use std::io;

fn main() -> io::Result<()> {
    match mounts::fetch_all_mounts() {
        Ok(found_mount_entries) => {
            for mountentry in found_mount_entries {
                println!("{mountentry:#?}")
            }
        }
        Err(error) => {
            eprintln!("failed to get all mounted filesystems: {error:?}")
        }
    }

    match mounts::get_mountpoint_fstype("/") {
        Ok(r#type) => println!("'/' fstype: {:?}", r#type),
        Err(err) => eprintln!("failed to get fstype of '/': {}", err.to_string()),
    }

    Ok(())
}
