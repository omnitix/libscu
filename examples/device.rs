// Required features: device

use libscu::hardware::device;

fn main() -> std::io::Result<()> {
    let device_model = device::fetch_model(true)?;
    println!("Device model: {device_model}");

    Ok(())
}
