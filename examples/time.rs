// Required features: uptime

use libscu::software::time;

fn print_time(_time: &libscu::types::Time, append_days: bool) {
    if append_days && _time.days > 0 {
        println!("  Days: {}", _time.days);
    }
    println!("  Hours: {}", _time.hours);
    println!("  Minutes: {}", _time.minutes);
    println!("  Seconds: {}", _time.seconds);
}

fn main() -> std::io::Result<()> {
    let uptime = time::fetch_uptime()?;

    println!("Uptime:");
    print_time(&uptime, true);

    Ok(())
}
