// Required features: proc

use libscu::software::proc;

fn main() {
    if let Ok(all_pids) = proc::list_pids() {
        println!("All pids of running processes: {all_pids:?}\n")
    };
    let pid = proc::get_self_pid();
    println!("Current process pid: {pid:?}");
    println!("Parent process pid: {:?}", proc::get_ppid(pid));
}
