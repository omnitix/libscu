// Required features: kernel

use libscu::software::kernel;

fn main() {
    let name = kernel::fetch_name();
    match name {
        Ok(name) => println!("Kernel name: {}", name),
        Err(error) => eprintln!("Failed to fetch kernel name: {error:?}"),
    }
    let version = kernel::fetch_version();
    match version {
        Ok(version) => println!("Kernel version: {}", version),
        Err(error) => eprintln!("Failed to fetch kernel version: {error:?}"),
    }
    let arch = kernel::fetch_arch();
    match arch {
        Ok(arch) => println!("Kernel arch: {}", arch),
        Err(error) => eprintln!("Failed to fetch kernel arch: {error:?}"),
    }
}
