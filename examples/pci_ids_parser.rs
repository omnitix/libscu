// Required features: pci_ids_parser

use libscu::util::pci_ids_parser::lookup_for_pci_id_value;

fn main() {
    println!("{:?}", lookup_for_pci_id_value("8086:5916"));
    println!("{:?}", lookup_for_pci_id_value("10DE:2206"));
}
