// Required features: os

use libscu::software::os;

fn main() {
    match os::fetch_release() {
        Ok(os_release) => {
            println!("Name: {}", os_release.name);
            println!("Pretty name: {}", os_release.pretty_name);
            if let Some(version) = os_release.version {
                println!("Version: {}", version);
            }
        }
        Err(error) => {
            println!("Cannot fetch OS release: {error:?}")
        }
    }
}
