fn main() {
    // use C code for macos from ll_c_code
    #[cfg(target_os = "macos")]
    {
        println!("cargo::rerun-if-changed=ll_c_code/macos/basic.c");
        cc::Build::new()
            .file("ll_c_code/macos/basic.c")
            .compile("macos_funcs.a");

        #[cfg(feature = "proc")]
        {
            println!("cargo::rerun-if-changed=ll_c_code/macos/proc.c");
            cc::Build::new()
                .file("ll_c_code/macos/proc.c")
                .compile("macos_proc.a");
        }

        #[cfg(feature = "terminal")]
        {
            println!("cargo::rerun-if-changed=ll_c_code/macos/term_size.c");
            cc::Build::new()
                .file("ll_c_code/macos/term_size.c")
                .compile("macos_term_size.a");
        }

        #[cfg(feature = "users")]
        {
            println!("cargo::rerun-if-changed=ll_c_code/macos/user.c");
            cc::Build::new()
                .file("ll_c_code/macos/user.c")
                .compile("macos_user.a");
        }

        // Apple silicon
        #[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
        {
            println!("cargo::rerun-if-changed=ll_c_code/macos/silicon_specific.c");
            println!("cargo::rustc-link-lib=framework=IOKit");
            println!("cargo::rustc-link-lib=framework=CoreFoundation");
            cc::Build::new()
                .file("ll_c_code/macos/silicon_specific.c")
                .compile("macos_silicon_funcs.a");
        }
    }

    // check feature conflicts
    #[cfg(all(feature = "pci_ids_parser", feature = "pci_ids"))]
    panic!("features `pci_ids_parser` and `pci_ids` cannot be enabled at the same time")
}
